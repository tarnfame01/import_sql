<?php
//header("Content-Type: application/vnd.ms-excel"); // ประเภทของไฟล์
//header('Content-Disposition: attachment; filename="myexcel.xls"'); //กำหนดชื่อไฟล์
//header("Content-Type: application/force-download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Transfer-Encoding: binary");
//header("Content-Length: ".filesize("myexcel.xls"));

@readfile($filename);
set_time_limit (60);
include '../connect.inc.php';
include '../../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('../document/34import_dividend_average_mmth_air.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');

$sheetData = $objPHPExcel->setActiveSheetIndex(0);
$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

$data = array();
$i = 0;
//echo '<pre>'; print_r($sheetData);exit;
$date_now = date("Y/m/d H:i:s", strtotime('+6 hour'));
$master_id = '4';
$year = '2021';
$dividend_percent = 4.6;
$average_percent = 7;
foreach ($sheetData as $key => $value) {
    if ($key > 3) {
//		if($key >= 18 && $key <= 20){
        if ($value['A'] == '') {
            break;
        }

//        $data[$i] = $value;
        $data[$i]['master_id'] = $master_id;
        $data[$i]['year'] = $year;
        $data[$i]['member_id'] = sprintf("%06d", trim($value['A']));
        $data[$i]['member_id'] = sprintf("%06d", trim($value['A']));
        $data[$i]['dividend_value'] = round(trim($value['E']),2);
        $data[$i]['average_return_value'] = round(trim($value['F']),2);
        $data[$i]['dividend_percent'] = $dividend_percent;
        $data[$i]['average_percent'] = $average_percent;
        $i++;
    }
}
//echo '<pre>'; print_r($data);
//exit;
$total_dividend = 0;
$total_average_return = 0;
foreach($data as $key => $value){
    $member_id = sprintf("%06d",$value['member_id']);
    $total_dividend += $data[$key]['dividend_value'];
    $total_average_return += $data[$key]['average_return_value'];
    $sql_update= "INSERT coop_dividend_average SET
        member_id = '".$member_id."',
        master_id = '".$master_id."',
        year = '".$value['year']."',
        dividend_value = '".sprintf("%.2f", $data[$key]['dividend_value'])."',
        average_return_value = '".sprintf("%.2f", $data[$key]['average_return_value'])."',
        gift_varchar = '0',
        dividend_percent = '".sprintf("%.2f", $data[$key]['dividend_percent'])."',
        average_percent = '".sprintf("%.2f", $data[$key]['average_percent'])."',
        date_create = '".$date_now."';";
    echo $sql_update.'<br>';
}

//foreach($data as $key => $value){
//    $member_id = sprintf("%06d",$value['member_id']);
//    $data[$key]['deduct_1'] = round($data[$key]['deduct_1'],2);
//    $data[$key]['deduct_2'] = round($data[$key]['deduct_2'],2);
//    $data[$key]['deduct_3'] = round($data[$key]['deduct_3'],2);
//    $data[$key]['deduct_4'] = round($data[$key]['deduct_4'],2);
//    $data[$key]['deduct_5'] = round($data[$key]['deduct_5'],2);
//    $data[$key]['deduct_6'] = round($data[$key]['deduct_6'],2);
//    $data[$key]['deduct_7'] = round($data[$key]['deduct_7'],2);
//    $data[$key]['deduct_8'] = round($data[$key]['deduct_8'],2);
//    if(!empty($data[$key]['deduct_1']) && $data[$key]['deduct_1'] != '-' && $data[$key]['deduct_1'] != '0'){
//        $sql_update= "INSERT coop_dividend_deduct SET
//        member_id = '".$member_id."',
//        master_id = '".$master_id."',
//        deduct_id = '1',
//        amount = '".$data[$key]['deduct_1']."',
//        date_create = '".$date_now."',
//        update_time = '".$date_now."';";
//        echo $sql_update.'<br>';
//    }
//    if(!empty($data[$key]['deduct_2']) && $data[$key]['deduct_2'] != '-'  && $data[$key]['deduct_2'] != '0'){
//        $sql_update= "INSERT coop_dividend_deduct SET
//        member_id = '".$member_id."',
//        master_id = '".$master_id."',
//        deduct_id = '2',
//        amount = '".$data[$key]['deduct_2']."',
//        date_create = '".$date_now."',
//        update_time = '".$date_now."';";
//        echo $sql_update.'<br>';
//    }
//    if(!empty($data[$key]['deduct_3']) && $data[$key]['deduct_3'] != '-'  && $data[$key]['deduct_3'] != '0' ){
//        $sql_update= "INSERT coop_dividend_deduct SET
//        member_id = '".$member_id."',
//        master_id = '".$master_id."',
//        deduct_id = '3',
//        amount = '".$data[$key]['deduct_3']."',
//        date_create = '".$date_now."',
//        update_time = '".$date_now."';";
//        echo $sql_update.'<br>';
//    }
//    if(!empty($data[$key]['deduct_4']) && $data[$key]['deduct_4'] != '-'  && $data[$key]['deduct_4'] != '0' ){
//        $sql_update= "INSERT coop_dividend_deduct SET
//        member_id = '".$member_id."',
//        master_id = '".$master_id."',
//        deduct_id = '4',
//        amount = '".$data[$key]['deduct_4']."',
//        date_create = '".$date_now."',
//        update_time = '".$date_now."';";
//        echo $sql_update.'<br>';
//    }
//    if(!empty($data[$key]['deduct_5']) && $data[$key]['deduct_5'] != '-'  && $data[$key]['deduct_5'] != '0' ){
//        $sql_update= "INSERT coop_dividend_deduct SET
//        member_id = '".$member_id."',
//        master_id = '".$master_id."',
//        deduct_id = '5',
//        amount = '".$data[$key]['deduct_5']."',
//        date_create = '".$date_now."',
//        update_time = '".$date_now."';";
//        echo $sql_update.'<br>';
//    }
//    if(!empty($data[$key]['deduct_6']) && $data[$key]['deduct_6'] != '-'  && $data[$key]['deduct_6'] != '0' ){
//        $sql_update= "INSERT coop_dividend_deduct SET
//        member_id = '".$member_id."',
//        master_id = '".$master_id."',
//        deduct_id = '6',
//        amount = '".$data[$key]['deduct_6']."',
//        date_create = '".$date_now."',
//        update_time = '".$date_now."';";
//        echo $sql_update.'<br>';
//    }
//    if(!empty($data[$key]['deduct_7']) && $data[$key]['deduct_7'] != '-' && $data[$key]['deduct_7'] != '0' ){
//        $sql_update= "INSERT coop_dividend_deduct SET
//        member_id = '".$member_id."',
//        master_id = '".$master_id."',
//        deduct_id = '7',
//        amount = '".$data[$key]['deduct_7']."',
//        date_create = '".$date_now."',
//        update_time = '".$date_now."';";
//        echo $sql_update.'<br>';
//    }
//    if(!empty($data[$key]['deduct_8']) && $data[$key]['deduct_8'] != '-' && $data[$key]['deduct_8'] != '0' ){
//        $sql_update= "INSERT coop_dividend_deduct SET
//        member_id = '".$member_id."',
//        master_id = '".$master_id."',
//        deduct_id = '8',
//        amount = '".$data[$key]['deduct_8']."',
//        date_create = '".$date_now."',
//        update_time = '".$date_now."';";
//        echo $sql_update.'<br>';
//    }
//}

$total_dividend = round($total_dividend,2);
$total_average_return = round($total_average_return,2);
$sql = "INSERT INTO `coop_dividend_average_master`(`id`, `year`, `dividend_value`, `average_return_value`, `dividend_percent`, `average_percent`, `status`, `gift_varchar`, `date_create`, `approve_date`, `udpate_time`) VALUES (".$master_id.", ".($year+543).", ".$total_dividend.", ".$total_average_return.", ".$dividend_percent.", ".$average_percent.", '0', 0, '".$date_now."', NULL, '".$date_now."');";
echo $sql.'<br>';



