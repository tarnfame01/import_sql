<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        ini_set("memory_limit","100000M");

    }
    public function index()
    {
        //echo "<pre>";print_r($_SESSION);echo"</pre>";
        $arr_data = array();
        $this->libraries->template('news',$arr_data);
    }


    public function import_data()
    {
        $this->db->select('*');
        $this->db->from('`P5_import`.`MEMBER`');
//        $this->db->limit('100');
        $MEMBER = $this->db->get()->result_array();

        $data_insert = array();
//        echo "<pre>";print_r($MEMBER);echo"</pre>";
        $i=0;
        $this->db->select('member_id');
        $this->db->from('coop_mem_apply');
        $coop_mem_apply_arr = $this->db->get()->result_array();

        $member_arr = array();
        foreach($coop_mem_apply_arr as $value2) {
            $member_arr[$value2['member_id']] = 1;
        }

        foreach ($MEMBER as $key => $value){
            $value['MEM_NO'] =  sprintf("%06d",$value['MEM_NO']);
            if($value['SEX'] == '1'){
                $value['SEX'] = 'M';
            }
            if($value['SEX'] == '2'){
                $value['SEX'] = 'F';
            }

            $value['TEL'] = str_replace('-','', $value['TEL']);

            $prename_id = $this->db->select('prename_id')
                ->from('p5coop_sys.coop_prename')
                ->where("prename_short = '".$value['PRENAM']."'")
                ->get()->row_array()['prename_id'];
            $mem_apply_id = sprintf("%06d",($i+1));


            $value['BRI_DAY'];
            $b_date = '';
            if($value['BRI_DAY'] != '543'){
                $b_date .= substr($value['BRI_DAY'] ,0, 4)-543;
                $b_date .= '-';
                $b_date .= substr($value['BRI_DAY'] ,4, 2);
                $b_date .= '-';
                $b_date .= substr($value['BRI_DAY'] ,6, 2);
            }
//            echo $b_date.'<br>';


            $data=array(
                'id' => ($i+1),
                'mem_apply_id' => $mem_apply_id,
                'member_id' => $value['MEM_NO'],
                'prename_id' => $prename_id,
                'firstname_th' => $value['FNAME'],
                'lastname_th' => $value['SNAME'],
                'sex' => $value['SEX'],
                'tel' => $value['TEL'],
                'salary' => $value['SALARY'],
//                'birthday' => '',
                'id_card' => $value['CARDID'],
                'share_month' => $value['STK_NUM'],
            );
            if(empty($member_arr[$data['member_id']])){
                $data_insert[$i] = $data;
            }

            if($value['MEM_NO'] == '000357'){
//                echo '<pre>'; print_r($value);exit;
            }
            if(!empty($value['SBDEP'])){
                $this->db->select('t2.*');
                $this->db->from('coop_mem_group as t1');
                $this->db->join('coop_mem_group as t2', "t1.id = t2.mem_group_parent_id AND t1.mem_group_type = '2'", "inner");
                $this->db->where("t1.mem_group_id = '".$value['DEPAR']."' AND t2.mem_group_id = '".$value['SBDEP']."'");
                $mem_group = $this->db->get()->row_array();
            }else{
                $this->db->select('t1.*');
                $this->db->from('coop_mem_group as t1');
                $this->db->where("t1.mem_group_id = '".$value['DEPAR']."' AND t1.mem_group_type = '2'");
                $mem_group = $this->db->get()->row_array();
            }
            if($value['MEM_NO'] == '000357'){
//                echo $this->db->last_query();exit;
            }
            $this->db->select('*');
            $this->db->from('`P5_import`.`MEMPOST`');
            $this->db->where("POST_NO = '".$value['POST_NO']."' AND TAMB_NO = '".$value['TAMB_NO']."'");
            $MEMPOST = $this->db->get()->row_array();
//            echo $value['MEM_NO'];
//            echo '<pre>'; print_r($MEMPOST);echo '</pre>';

            $this->db->select('*');
            $this->db->from('coop_province');
            $this->db->where("province_name = '".$MEMPOST['PROVIN']."'");
            $coop_province = $this->db->get()->row_array();
//            echo '<pre>'; print_r($coop_province);echo '</pre>';
//            echo '<pre>'; print_r($value);exit;

            $coop_amphur=array();
            $coop_district=array();
            if(!empty($coop_province)){
                $this->db->select('*');
                $this->db->from('coop_amphur');
                $this->db->where("amphur_name = '".$MEMPOST['AMPOR']."' AND province_id = '".$coop_province['province_id']."'");
                $coop_amphur = $this->db->get()->row_array();
//                echo '<pre>'; print_r($coop_amphur);echo '</pre>';
            }

            if(!empty($coop_amphur)){
                $this->db->select('*');
                $this->db->from('coop_district');
                $this->db->where("district_name = '".$MEMPOST['TAMBOL']."' AND amphur_id = '".$coop_amphur['amphur_id']."'");
                $coop_district = $this->db->get()->row_array();
//                echo '<pre>'; print_r($coop_district);echo '</pre>';
            }
            $mb_date = '';
            $value['MB_DATE'];
            if($value['MB_DATE'] != '543'){
                $mb_date .= substr($value['MB_DATE'] ,0, 4)-543;
                $mb_date .= '-';
                $mb_date .= substr($value['MB_DATE'] ,4, 2);
                $mb_date .= '-';
                $mb_date .= substr($value['MB_DATE'] ,6, 2);
            }
//            echo $mb_date.'<br>';

            $ex_date = '';
//            echo $value['EXP_DATE'].'<br>';
            if($value['FL_EXP'] == '*'){
                $mem_type = '2';
                if($value['EXP_DATE'] != '543'){
                    $ex_date .= substr($value['EXP_DATE'] ,0, 4)-543;
                    $ex_date .= '-';
                    $ex_date .= substr($value['EXP_DATE'] ,4, 2);
                    $ex_date .= '-';
                    $ex_date .= substr($value['EXP_DATE'] ,6, 2);
                }
//                echo $ex_date.'<br>';

            }else{
                $mem_type = '1';
            }
//            echo $mem_type.'<br>';
//            echo '<br>';

            if(!empty($value['SBDEP'])) {
                $data = array(
                    'member_id' => $value['MEM_NO'],
                    'birthday' => $b_date,
                    'department' => '1',
                    'faction' => $mem_group['mem_group_parent_id'],
                    'level' => $mem_group['id'],
                    'address_no' => $value['ADDRESS'],
                    'province_id' => $coop_province['province_id'],
                    'amphur_id' => $coop_amphur['amphur_id'],
                    'district_id' => $coop_district['district_id'],
                    'apply_type_id' => '1',
                    'mem_type' => $mem_type,
                    'apply_date' => $mb_date,
                    'member_status' => $mem_type,
                );
            }else{
                $data = array(
                    'member_id' => $value['MEM_NO'],
                    'birthday' => $b_date,
                    'department' => $mem_group['mem_group_parent_id'],
                    'faction' => $mem_group['id'],
                    'level' => '-',
                    'address_no' => $value['ADDRESS'],
                    'province_id' => $coop_province['province_id'],
                    'amphur_id' => $coop_amphur['amphur_id'],
                    'district_id' => $coop_district['district_id'],
                    'apply_type_id' => '1',
                    'mem_type' => $mem_type,
                    'apply_date' => $mb_date,
                    'member_status' => $mem_type,
                );
            }
//            echo $this->db->last_query();
//            echo '<pre>'; print_r($data);exit;
            $data_update[$i] = $data;
            $i++;

        }

//        echo '<pre>'; print_r($data_insert);exit;


//        $this->db->insert_batch('coop_mem_apply', $data_insert);
//        echo '<pre>'; print_r($data_insert);exit;

//        $this->db->update_batch('coop_mem_apply', $data_update, 'member_id');
//        echo '<pre>'; print_r($data_update);exit;
        echo 'Ok';

    }

    public function coop_mem_share()
    {
        $this->db->select('*');
        $this->db->from('`P5_import`.`STKPAY`');
//        $this->db->where("`PAY_DATE` > '25620000'");
//        $this->db->where("`PAY_DATE` > '25540000' AND `PAY_DATE` <= '25550000' AND MEM_NO = 9");
//        $this->db->limit('1000');
        $STKPAY = $this->db->get()->result_array();
//        echo '<pre>'; print_r($STKPAY);echo '</pre>';
//        exit;
        $i = 0;
        $i++;
        $data_insert = array();
        foreach ($STKPAY as $key => $value){
            $i++;
            $value['MEM_NO'] =  sprintf("%06d",$value['MEM_NO']);
            $s_date = '';
            if($value['BRI_DAY'] != '543'){
                $s_date .= substr($value['PAY_DATE'] ,0, 4)-543;
                $s_date .= '-';
                $s_date .= substr($value['PAY_DATE'] ,4, 2);
                $s_date .= '-';
                $s_date .= substr($value['PAY_DATE'] ,6, 2);
                $s_date .= ' 00:00:00';
            }
//            echo $s_date.'<br>';
            $share_type = 'SPM';
            if($value['FL_PAY'] == 'P2'){
                $share_type = 'SPA';
            }
            if($value['FL_PAY'] == 'P3'){
                $share_type = 'SRP';
            }

            $data=array(
                'share_id' => null,
                'member_id' => $value['MEM_NO'],
                'admin_id' => '1',
                'share_type' => $share_type,
                'share_date' => $s_date,
                'share_status' => '1',
                'share_early' => $value['STK_NUM'],
                'share_early_value' => $value['STK_AMT'],
                'share_collect' => $value['SUM_STK'],
                'share_collect_value' => ($value['SUM_STK']*$value['STK_PRI']),
                'share_value' => $value['STK_PRI'],
                'share_bill_date' => $s_date,
                'share_period' => $value['LSTTERM'],
                'share_bill' => $value['BILLNUM'],
            );
            $data_insert[$i] = $data;
        }
        $this->db->insert_batch('coop_mem_share', $data_insert);
//        echo '<pre>'; print_r($data_insert);
    }

    public function coop_mem_group()
    {
        $this->db->select('*');
        $this->db->from('`P5_import`.`GRPCLS`');
        $this->db->where("`GRPCLS` = '2'");
        $this->db->order_by("GRPCOD, DEPCOD");
//        $this->db->limit('1000');
        $CLSNAM = $this->db->get()->result_array();
//        echo '<pre>'; print_r($CLSNAM);exit;
//        echo '<pre>';
        $i = 9;
        foreach ($CLSNAM as $key => $value){
            $this->db->select('*');
            $this->db->from('`P5_import`.`GRPCLS`');
            $this->db->where("`GRPCLS` = 'D' AND `GRPCOD` = '".$value['DEPCOD']."'");
            $this->db->order_by("DEPCOD");
            $sub_group = $this->db->get()->result_array();
            $CLSNAM[$key]['sub_group'] = $sub_group;

//            if(!empty($sub_group)){
            $i ++;
            $data=array(
                'id'                    => $i,
                'mem_group_id'          => $value['DEPCOD'],
                'mem_group_parent_id'   => '1',
                'mem_group_type'        => '2',
                'mem_group_name'        => $value['CLSNAM'],
                'mem_group_full_name'   => $value['CLSNAM'],
            );
            $CLSNAM[$key]['parent_id'] = $i;
            $data_insert[$i] = $data;
//            }

        }
        foreach ($CLSNAM as $qwe => $datas){
            foreach ($datas['sub_group'] as $key => $value){
                $i ++;
                $data=array(
                    'id'                    => $i,
                    'mem_group_id'          => $value['DEPCOD'],
                    'mem_group_parent_id'   => $datas['parent_id'],
                    'mem_group_type'        => '3',
                    'mem_group_name'        => $value['CLSNAM'],
                    'mem_group_full_name'   => $value['CLSNAM'],
                );
                $data_insert[$i] = $data;
            }
        }
        echo '<pre>';
        print_r($CLSNAM);
        print_r($data_insert);
        echo '</pre>';
//        exit;
//        $this->db->insert_batch('coop_mem_group', $data_insert);
    }

    public function new_coop_mem_group()
    {
        $this->db->select('MEM_NO, DEPAR, SBDEP');
        $this->db->from('`P5_import`.`MEMBER`');
//        $this->db->limit('1000');
        $MEMBER = $this->db->get()->result_array();


        foreach ($MEMBER as $key => $value){
            if(empty($value['SBDEP'])){
                $this->db->select('*');
                $this->db->from('`P5_import`.`GRPCLS`');
                $this->db->where("`GRPCOD` = 'XX' AND `DEPCOD` = '".$value['DEPAR']."'");
                $sub_group = $this->db->get()->result_array();
                echo $value['MEM_NO'].'<br>';
                echo $this->db->last_query().'<br>';
                echo $sub_group[0]['CLSNAM'].'<br>';
            }else{
//                $this->db->select('*');
//                $this->db->from('`P5_import`.`GRPCLS`');
//                $this->db->where("`GRPCLS` = '".$value['MEM_NO']."'");
//                $this->db->order_by("TRF_DATE DESC");
//                $sub_group = $this->db->get()->result_array();
//                echo $this->db->last_query().'<br>';
            }
        }
    }

    public function new_coop_finance_transaction()
    {
        $this->db->select('*');
        $this->db->from('`P5_import`.`STKPAY`');
//        $this->db->where("`PAY_DATE` > '25620000'");
//        $this->db->where("`MEM_NO` = '32'");
//        $this->db->limit(100);
//        $this->db->where("`PAY_DATE` > '25540000' AND `PAY_DATE` <= '25550000' AND MEM_NO = 9");
//        $this->db->limit('1000');
        $STKPAY = $this->db->get()->result_array();
//        echo $this->db->last_query();
//        echo '<pre>'; print_r($STKPAY);echo '</pre>';
//        exit;
        exit;
        $i = 0;
        $i++;
        $data_insert = array();
        foreach ($STKPAY as $key => $value){
            $i++;
            $value['MEM_NO'] =  sprintf("%06d",$value['MEM_NO']);
            $s_date = '';
            if($value['BRI_DAY'] != '543'){
                $s_date .= substr($value['PAY_DATE'] ,0, 4)-543;
                $s_date .= '-';
                $s_date .= substr($value['PAY_DATE'] ,4, 2);
                $s_date .= '-';
                $s_date .= substr($value['PAY_DATE'] ,6, 2);
                $s_date .= ' 00:00:00';
            }
//            echo $s_date.'<br>';
            $share_type = 'SPM';
            if($value['FL_PAY'] == 'P2'){
                $share_type = 'SPA';
            }
            if($value['FL_PAY'] == 'P3'){
                $share_type = 'SRP';
            }

//            $data=array(
//                'share_id' => null,
//                'member_id' => $value['MEM_NO'],
//                'admin_id' => '1',
//                'share_type' => $share_type,
//                'share_date' => $s_date,
//                'share_status' => '1',
//                'share_early' => $value['STK_NUM'],
//                'share_early_value' => $value['STK_AMT'],
//                'share_collect' => $value['SUM_STK'],
//                'share_collect_value' => ($value['SUM_STK']*$value['STK_PRI']),
//                'share_value' => $value['STK_PRI'],
//                'share_bill_date' => $s_date,
//                'share_period' => $value['LSTTERM'],
//                'share_bill' => $value['BILLNUM'],
//            );

            if(!empty($value['BILLNUM'])) {

                $this->db->select('*');
                $this->db->from('coop_receipt');
                $this->db->where("`receipt_id` = '" . $value['BILLNUM'] . "'");
                $this->db->limit(1);
                $receipt_check = $this->db->get()->row_array();
//            exit;
//                echo $this->db->last_query();
//                exit;
//                echo '<pre>'; print_r($receipt_check);echo '</pre>';
                if (!empty($receipt_check)) {
//                    echo 'asdasdasdasd';
//                    echo '- -' . $value['BILLNUM'] . exit;
//                echo $receipt_check['sumcount'].' + '.$value['STK_AMT'].'<br>';
                    $sql = "UPDATE `coop_receipt` SET `sumcount` = " . ($receipt_check['sumcount'] + $value['STK_AMT']) . " WHERE `receipt_id` = '" . $value['BILLNUM'] . "';";
//                    echo $sql . '<br>';
                    $this->db->query($sql);
                    $sql = "INSERT INTO `coop_finance_transaction`(`member_id`, `receipt_id`, `loan_id`, `account_list_id`, `principal_payment`, `interest`, `total_amount`, `payment_date`, `createdatetime`, `period_count`, `loan_amount_balance`, `loan_interest_remain`, `finance_transaction_id`, `cremation_type_id`, `transaction_text`, `loan_atm_id`, `deduct_type`, `process`, `interest_cal`, `interest_from`, `interest_to`, `interest_arrear`, `interest_over`, `interest_calculate`, `item_type_code`) VALUES ('" . $value['MEM_NO'] . "', '" . $value['BILLNUM'] . "', null, '14', '" . $value['STK_AMT'] . "', null, " . $value['STK_AMT'] . ", '" . $s_date . "', '" . $s_date . "', '" . $value['LSTTERM'] . "', NULL, NULL, NULL, NULL, 'หุ้นรายเดือน', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)";
//                    echo $sql . '<br>' . '<hr>';
                    $this->db->query($sql);
                } else {
                    if (!empty($value['BILLNUM'])) {
//                        echo 'asdasdasdasd';
//                        echo '- -' . $value['BILLNUM'].'<hr>';

                        $sql = "INSERT INTO `coop_receipt`(`receipt_id`, `member_id`, `sumcount`, `receipt_datetime`, `receipt_status`, `cancel_date`, `admin_id`, `month_receipt`, `year_receipt`, `finance_month_profile_id`, `pay_type`, `order_by`, `cancel_by`) VALUES ('".$value['BILLNUM']."', '".$value['MEM_NO']."', '".$value['STK_AMT']."', '".$s_date."', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);";
//                        echo $sql . '<br>';
                        $this->db->query($sql);
                        $sql = "INSERT INTO `coop_finance_transaction`(`member_id`, `receipt_id`, `loan_id`, `account_list_id`, `principal_payment`, `interest`, `total_amount`, `payment_date`, `createdatetime`, `period_count`, `loan_amount_balance`, `loan_interest_remain`, `finance_transaction_id`, `cremation_type_id`, `transaction_text`, `loan_atm_id`, `deduct_type`, `process`, `interest_cal`, `interest_from`, `interest_to`, `interest_arrear`, `interest_over`, `interest_calculate`, `item_type_code`) VALUES ('" . $value['MEM_NO'] . "', '" . $value['BILLNUM'] . "', null, '14', '" . $value['STK_AMT'] . "', null, " . $value['STK_AMT'] . ", '" . $s_date . "', '" . $s_date . "', '" . $value['LSTTERM'] . "', NULL, NULL, NULL, NULL, 'หุ้นรายเดือน', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)";
//                        echo $sql . '<br>' . '<hr>';
                        $this->db->query($sql);
//                        exit;

                    }
                }
//            exit;
//                $data_insert[$i] = $data;
            }
        }
//        $this->db->insert_batch('coop_mem_share', $data_insert);
//        echo '<pre>'; print_r($data_insert);

    }

    public function new_status_member()
    {
        exit;
        $this->db->select('*');
        $this->db->from('`P5_import`.`MEMBER`');
        $STKPAY = $this->db->get()->result_array();
        $i = 0;
        $i++;
        foreach ($STKPAY as $key => $value){
            $i++;
            $value['MEM_NO'] =  sprintf("%06d",$value['MEM_NO']);
            if($value['DEPAR'] == 'A1'){
                $sql = "UPDATE `p5coop_sys`.`coop_mem_apply` SET `mem_type_id` = '2' WHERE `member_id` = '".$value['MEM_NO']."'";
            }else{
                $sql = "UPDATE `p5coop_sys`.`coop_mem_apply` SET `mem_type_id` = '1' WHERE `member_id` = '".$value['MEM_NO']."'";
            }
            $this->db->query($sql);

        }
//        $this->db->insert_batch('coop_mem_share', $data_insert);
//        echo '<pre>'; print_r($data_insert);

    }

    public function import_mem_position()
    {
        exit;
        // ส่วน update coop_mem_position
        /*
        $this->db->select('*');
        $this->db->from('`P5_import`.`GRPSET`');
        $this->db->where("`GRP` = 'P'");
        $this->db->order_by('DEPAR');
        $STKPAY = $this->db->get()->result_array();
        $i = 0;
        $i++;
//        echo '<pre>'; print_r($STKPAY);exit;
        foreach ($STKPAY as $key => $value){
            $i++;
            $value['MEM_NO'] =  sprintf("%06d",$value['MEM_NO']);
            $sql = "INSERT INTO `p5coop_sys`.`coop_mem_position`(`position_name`, `position_number`) VALUES ('".$value['DEP_NAME']."', '".$value['DEPAR']."')";
            $this->db->query($sql);
        }
        */
        // end

        $this->db->select('MEM_NO, POSIT');
        $this->db->from('`P5_import`.`MEMBER`');
        $this->db->where('`POSIT` is not null');
        $MEMBER = $this->db->get()->result_array();
//        echo '<pre>'; print_r($MEMBER);exit;
        foreach ($MEMBER as $key => $value){
            $this->db->select('*');
            $this->db->from('coop_mem_position');
            $this->db->where("`position_number` = '".$value['POSIT']."'");
            $mem_position = $this->db->get()->row_array();
            $value['MEM_NO'] =  sprintf("%06d",$value['MEM_NO']);

            $sql = "UPDATE `coop_mem_apply` SET `position_id` = '".$mem_position['position_id']."' WHERE `member_id` = '".$value['MEM_NO']."';";
            echo $sql.'<br>';
            $this->db->query($sql);
//            echo '<pre>'; print_r($value);exit;
        }



    }


    public function import_coop_mem_gain()
    {
        $this->db->select('SRVTYP, MEM_NO, TRNDAT, LINNUM, RCVNAM, RELNAM');
        $this->db->from('`P5_import`.`MEBOTH`');
//        $this->db->where('RELNAM is not null');
        $mem_gain = $this->db->get()->result_array();
//        echo '<pre>'; print_r($mem_gain);echo '<pre>';

        $this->db->select('relation_id, relation_name');
        $this->db->from('`coop_mem_relation`');
        $mem_relation_arr = $this->db->get()->result_array();
        $mem_relation = array();
        foreach ($mem_relation_arr as $value){
            $mem_relation[$value['relation_name']] = $value['relation_id'];
        }

        $this->db->select('prename_full, prename_short, prename_id');
        $this->db->from('`coop_prename`');
        $this->db->order_by('`prename_id`', 'DESC');
        $coop_prename_arr = $this->db->get()->result_array();
        $coop_prename = array();
        foreach ($coop_prename_arr as $value){
            $coop_prename[$value['prename_id']]['prename_full'] = $value['prename_full'];
            $coop_prename[$value['prename_id']]['prename_short'] = $value['prename_short'];
        }
//        echo '<pre>'; print_r($mem_relation);echo '</pre>';
//        exit;
//        echo '<hr>';
        foreach ($mem_gain as $key => $value){
            $insert_data = array();
            $insert_data['member_id'] = sprintf("%06d",$value['MEM_NO']);
            $s_date = '';
            if($value['TRNDAT'] != '543'){
                $s_date .= substr($value['TRNDAT'] ,0, 4)-543;
                $s_date .= '-';
                $s_date .= substr($value['TRNDAT'] ,4, 2);
                $s_date .= '-';
                $s_date .= substr($value['TRNDAT'] ,6, 2);
                $s_date .= ' 00:00:00';
            }
            $insert_data['g_create'] = $s_date;
            $insert_data['seq'] = $value['LINNUM'];
            $insert_data['full_name'] = $value['RCVNAM'];
            $insert_data['RELNAM'] = $value['RELNAM'];


            $RELNAM = str_replace('บุตรสาว',"บุตร",$value['RELNAM']);
            $RELNAM = str_replace('บุตรชาย',"บุตร",$RELNAM);
            $RELNAM = str_replace('มาดา',"มารดา",$RELNAM);
            $RELNAM = str_replace('ลูกพี่ลูกน้อง',"ญาติ",$RELNAM);
            $RELNAM = str_replace('พ่อ',"บิดา",$RELNAM);
            $RELNAM = str_replace('แม่',"มารดา",$RELNAM);
            $RELNAM = str_replace('หลานชาย',"หลาน",$RELNAM);
            $RELNAM = str_replace('หลานสาว',"หลาน",$RELNAM);
            $RELNAM = str_replace('ธิดา',"บุตร",$RELNAM);
            $RELNAM = str_replace(' (ลำดับ1)',"",$RELNAM);
            $RELNAM = str_replace(' (ลำดับ2)',"",$RELNAM);
            $RELNAM = str_replace(' ลำดับ 1',"",$RELNAM);
            $RELNAM = str_replace(' ลำดับ 2',"",$RELNAM);
            $RELNAM = str_replace(' 70/100',"",$RELNAM);
            $RELNAM = str_replace(' (1)',"",$RELNAM);
            $RELNAM = str_replace('            ด',"",$RELNAM);
            $RELNAM = str_replace('+',"",$RELNAM);
            $RELNAM = str_replace('-',"",$RELNAM);
            $RELNAM = str_replace('.',"",$RELNAM);
            $RELNAM = str_replace("\\","",$RELNAM);
            $RELNAM = str_replace('มารดาที่ชอบด้วยกฎหมาย',"มารดา",$RELNAM);
            $RELNAM = str_replace('(ลำดับ1 70%)',"",$RELNAM);
            $RELNAM = str_replace('บิดา-มารดา',"บิดาและมารดา",$RELNAM);
            $RELNAM = str_replace('บดา',"บิดา",$RELNAM);
//            $RELNAM = str_replace('ตนเอง',"",$RELNAM);
            $RELNAM = str_replace('คู่สมรส',"คู่สมรส",$RELNAM);
            $RELNAM = str_replace('เหลนชาย',"หลาน",$RELNAM);
            $RELNAM = str_replace('เหลนสาว',"หลาน",$RELNAM);
            $RELNAM = str_replace('หลานสาว',"หลาน",$RELNAM);
            $RELNAM = str_replace('หลานชาย',"หลาน",$RELNAM);
            $RELNAM = str_replace('เหลน',"หลาน",$RELNAM);
            $RELNAM = str_replace('ญาติสนิท',"ญาติ",$RELNAM);
            $RELNAM = str_replace('มารดาภรรยา',"มารดาของภรรยา",$RELNAM);
            $RELNAM = str_replace('ลูก',"บุตร",$RELNAM);
            $RELNAM = str_replace('คู่หมั้น',"คู่สมรส",$RELNAM);
            $RELNAM = str_replace('พี่สาวภรรยา',"พี่สะใภ้",$RELNAM);
            $RELNAM = str_replace('น้องสาวภรรยา',"น้องสะใภ้",$RELNAM);
            $RELNAM = str_replace('ภริยา',"ภรรยา",$RELNAM);
            $RELNAM = str_replace('รรร',"รร",$RELNAM);
            $RELNAM = str_replace('ลุตร',"บุตร",$RELNAM);
            $RELNAM = str_replace('น้องชายต่างบิดา',"น้องชาย",$RELNAM);
            $RELNAM = str_replace('พี่สว',"พี่สาว",$RELNAM);
            $RELNAM = str_replace('พี่น้องร่วมบิดา-มารดา',"พี่น้อง",$RELNAM);
            $RELNAM = str_replace('พี่น้องร่วมบิดามารดา',"พี่น้อง",$RELNAM);
            $RELNAM = str_replace('พี่น้องร่วมมารดา',"พี่น้อง",$RELNAM);
            $RELNAM = str_replace('มารดา',"มารดา",$RELNAM);
            $RELNAM = str_replace('มารดาภรรยา',"มารดาของภรรยา",$RELNAM);
            $RELNAM = str_replace('ที่ชอบด้วยกฎหมาย',"",$RELNAM);
            $RELNAM = str_replace('บิด',"บิดา",$RELNAM);
            $RELNAM = str_replace('าา',"า",$RELNAM);
            $RELNAM = str_replace('แฟนสาว',"ภรรยา",$RELNAM);
            $RELNAM = str_replace('บิดา มารดา',"บิดาและมารดา",$RELNAM);
            $RELNAM = str_replace('บิดามารดา',"บิดาและมารดา",$RELNAM);
            $RELNAM = str_replace('son',"บุตร",$RELNAM);
            $RELNAM = str_replace('husband',"สามี",$RELNAM);
            $RELNAM = str_replace('น้าสาว',"น้า",$RELNAM);
            $RELNAM = str_replace('บุตา',"บุตร",$RELNAM);
            $RELNAM = str_replace('บิาด',"บิดา",$RELNAM);
            $RELNAM = str_replace('มารดาด',"มารดา",$RELNAM);
            $RELNAM = str_replace('บุต',"บุตร",$RELNAM);
            $RELNAM = str_replace('ตรร',"ตร",$RELNAM);
            $RELNAM = str_replace('เพื่อนสาว',"เพื่อน",$RELNAM);
            $RELNAM = str_replace(' ',"",$RELNAM);
            $RELNAM = str_replace('(ลำดับที่1เสีย)',"",$RELNAM);
            $RELNAM = str_replace('(ลำดับที่2เสีย)',"",$RELNAM);
            $RELNAM = str_replace('(รับ2/3)',"",$RELNAM);
            $RELNAM = str_replace('(รับ1/3)',"",$RELNAM);
            $RELNAM = str_replace('ลำดับที่1',"",$RELNAM);
            $RELNAM = str_replace('ลำดับที่2',"",$RELNAM);
            $RELNAM = str_replace('(1)',"",$RELNAM);
            $RELNAM = str_replace('(2)',"",$RELNAM);
            $RELNAM = str_replace('30/100',"",$RELNAM);
            $RELNAM = str_replace('(ลำดับที่1)',"",$RELNAM);
            $RELNAM = str_replace('(ลำดับที่2)',"",$RELNAM);
            $RELNAM = str_replace('(ลำดับ1)',"",$RELNAM);
            $RELNAM = str_replace('(ลำดับ2)',"",$RELNAM);
            $RELNAM = str_replace('บุตรบุตร',"บุตร",$RELNAM);
            $RELNAM = str_replace('บุตรธรรม',"บุตรบุญธรรม",$RELNAM);
            $RELNAM = str_replace('บุรา',"บุตร",$RELNAM);
            $RELNAM = str_replace('น้าชาย',"น้า",$RELNAM);
            $RELNAM = str_replace('น้อย',"น้อง",$RELNAM);
            $RELNAM = str_replace('บุตรสาว',"บุตร",$RELNAM);
            $RELNAM = str_replace('เพื่อนสนิท',"เพื่อน",$RELNAM);
            $RELNAM = str_replace('1',"",$RELNAM);
            $RELNAM = str_replace('3',"",$RELNAM);
            $RELNAM = str_replace('0',"",$RELNAM);
            $RELNAM = str_replace('%',"",$RELNAM);
            $RELNAM = str_replace('ลำดับ',"",$RELNAM);
            $RELNAM = str_replace('บุตรชาย',"บุตร",$RELNAM);
            $RELNAM = str_replace('()',"",$RELNAM);
            $RELNAM = str_replace('บตร',"บุตร",$RELNAM);
            $RELNAM = str_replace('บุตรสะใภ้',"บุตร",$RELNAM);
            $RELNAM = str_replace('-',"",$RELNAM);



            $insert_data['relation_id'] = $mem_relation[$RELNAM];



//            echo  '<pre>'; print_r($insert_data);echo '<pre>';
//            echo $insert_data['full_name'].'<br>';
            $affix = array('หากลำดับที่ 1 เสียชีวิตก่อนจะมอบให้', 'ลำดับที่ 2', 'ติดต่อ 70/8 ม.1 ต.พิชัย อ.เมือง', 'ต.ป่าสัก จ.ลำพูน 10%', 'บริจาคให้ 10% ของยอดเงินที่ได้รับสุทธิ', 'จ.ลำปาง', 'ให้จ่ายเงินทั้งหมดแก่ผู้รับโอน', 'ประโยชน์ที่ยังมีชีวิตก่อน', '*จ่ายเงินทั้งหมดแก่ทายาทที่ยังมีชีวิตในล'); // ตัดคำออก
            if(in_array($insert_data['full_name'], $affix)){
                continue;
            }

            $full_name = str_replace('  '," ",$insert_data['full_name']);
            $full_name = str_replace('  '," ",$full_name);
            $full_name = str_replace('  '," ",$full_name);
            $full_name = str_replace('  '," ",$full_name);
            $full_name = str_replace('  '," ",$full_name);
            $full_name = str_replace('พลตรีหญิง ',"พลตรีหญิง",$full_name);
            $full_name = str_replace('ว่าที่ ',"ว่าที่",$full_name);
            $full_name = str_replace('นางสาว ',"นางสาว",$full_name);
            $full_name = str_replace('น.ร. ',"น.ร.",$full_name);
            $full_name = str_replace('พลเอก ',"พลเอก",$full_name);
            $full_name = str_replace('ว่าที่ร้อยตรีหญิง ',"ว่าที่ร้อยตรีหญิง",$full_name);
            $full_name = str_replace('น.ส ',"น.ส",$full_name);
            $full_name = str_replace('พล.ต.หญิง ',"พล.ต.หญิง",$full_name);
            $full_name = str_replace('พ.ต.ต ',"พ.ต.ต",$full_name);
            $full_name = explode(" ",$full_name);
            $prename_id = null;

            $middle_name = array('ณ', 'ปริญญา', 'กุญษร', 'เจมส์', 'วิลเลี่ยม', 'รวมชัย', 'รัตนถาวรและด.ช.ภัทร', 'วีกาญจน์', 'ศศิร์อร'); // ชื่อกลาง
            if(in_array($full_name[1], $middle_name)){
//                echo '<pre>'; print_r($full_name);
                $full_name[0] = $full_name[0];
                $full_name[1] = $full_name[1].' '.$full_name[2];
                unset($full_name[2]);
//                echo '<pre>'; print_r($full_name);exit;
            }
            $affix = array(
                '(20%)',
                '31-0008',
                '03-00105',
                '(อ่านไม่ออก)',
                '(ค่าหุ้น)',
                '50%',
                'โทร.09022799990',
                'ให้20%',
                'รน.',
                'และ',
                'ภ'
            ); // ตัดคำออก
            if(in_array($full_name[2], $affix)){
                unset($full_name[2]);
            }

            if(!empty($full_name[2])){
                $prename = $full_name[0];
                $firstname_th = $full_name[1];
                $lastname_th = $full_name[2];
                if(!empty($full_name[3])){
                    $lastname_th .= $full_name[3];
                    if(!empty($full_name[4])){
                        $lastname_th .= $full_name[4];
                        if(!empty($full_name[5])){
                            $lastname_th .= $full_name[5];
                        }
                    }
                }
                foreach ($coop_prename as $key2 => $value2){
                    if($prename == $value2['prename_short']){
                        $prename_id = $key2;
                    }else if($prename == $value2['prename_short']){
                        $prename_id = $key2;
                    }
                }
                if(empty($prename_id)) {
//                    echo $prename_id . ' | ' . $prename . ' | ' . $firstname_th . ' | ' . $lastname_th . '<hr>';
                }
            }else{
                $prename = '';
                $firstname_th = $full_name[0];
                $lastname_th = $full_name[1];

                foreach ($coop_prename as $key2 => $value2){
//                    echo $value2['prename_full'] . '!=' .$firstname_th.'<br>';
                    $pos = strrpos( $firstname_th, $value2['prename_short'] );

                    if( $pos===false ) {
//                        echo '-------'.'<br>';
                    } else {
//                        echo $value2['prename_short'].'<br>';
                        $prename = $value2['prename_short'];
                        $prename_id = $key2;
                        $firstname_th = str_replace($prename,'',$firstname_th);
                        break ;
                    }
                }

                if(empty($prename)) {
                    foreach ($coop_prename as $key2 => $value2) {
//                    echo $value2['prename_full'] . '!=' .$firstname_th.'<br>';
                        $pos = strrpos($firstname_th, $value2['prename_full']);

                        if ($pos === false) {
//                        echo '-------'.'<br>';
                        } else {
//                            echo $value2['prename_full'] . '<br>';
                            $prename = $value2['prename_full'];
                            $prename_id = $key2;
                            $firstname_th = str_replace($prename, '', $firstname_th);
                            break;
                        }
                    }
                }
                if(empty($prename_id)){ //check 1
                    $firstname_th = str_replace('นส.',"น.ส.",$firstname_th);
                    $firstname_th = str_replace('ดช.',"ด.ช.",$firstname_th);
                    $firstname_th = str_replace('ดญ.',"ด.ญ.",$firstname_th);
                    $firstname_th = str_replace('น.ต.',"นต.",$firstname_th);
                    $firstname_th = str_replace('ดข.',"ด.ช.",$firstname_th);
                    $firstname_th = str_replace('ด.ข.',"ด.ช.",$firstname_th);
                    $firstname_th = str_replace('ดต.',"ด.ต.",$firstname_th);
                    $firstname_th = str_replace('น.ส',"น.ส.",$firstname_th);
                    foreach ($coop_prename as $key2 => $value2){
                        $pos = strrpos( $firstname_th, $value2['prename_short'] );
                        if( $pos===false ) {
                        } else {
                            $prename = $value2['prename_short'];
                            $prename_id = $key2;
                            $firstname_th = str_replace($prename,'',$firstname_th);
                            break ;
                        }
                    }

                    if(empty($prename)) {
                        foreach ($coop_prename as $key2 => $value2) {
                            $pos = strrpos($firstname_th, $value2['prename_full']);

                            if ($pos === false) {
                            } else {
                                $prename = $value2['prename_full'];
                                $prename_id = $key2;
                                $firstname_th = str_replace($prename, '', $firstname_th);
                                break;
                            }
                        }
                    }
//                    echo $prename_id.' '.$prename.' '.$firstname_th.' '.$lastname_th.'<hr>';
                    if(empty($prename_id)){ //check 2
//                        echo $prename_id.' '.$prename.' '.$firstname_th.' '.$lastname_th.'<hr>';

//                        echo $prename_id . ' | ' . $prename . ' | ' . $firstname_th . ' | ' . $lastname_th . '<hr>';
                    }
                }
//                echo $prename_id.' '.$prename.' '.$firstname_th.' '.$lastname_th.'<hr>';
            }
            if(empty($insert_data['relation_id'])){
                $insert_data['relation_id'] = 'null';
//                echo $value['MEM_NO'].' '.$firstname_th.' '.$insert_data['relation_id'].' '.$RELNAM.'<hr>';
            }
            if(empty($prename_id)) {
                $prename_id = 'null';
            }

            $firstname_th = str_replace('\\',"",$firstname_th);
            $lastname_th = str_replace('\\',"",$lastname_th);

            $sql = "INSERT INTO `coop_mem_gain_detail`(`member_id`, `seq`, `g_prename_id`, `g_firstname`, `g_lastname`, `g_create`, `g_relation_id`) VALUES ('".$insert_data['member_id']."', '".$value['LINNUM']."', ".$prename_id.", '".$firstname_th."', '".$lastname_th."', '".$s_date."', ".$insert_data['relation_id'].");";
            echo $sql.'<br>';
//            $this->db->query($sql);
        }



    }


    public function import_mem_bank()
    {
        $this->db->select('t1.MEM_NO, t1.BNKCOD, t1.ACCNUM, t1.BRN_NAM, t1.RMK_ACC');
        $this->db->from('`P5_import`.`MBACCT` as t1');
//        $this->db->join('`P5_import`.`GRPSET` as t2',"t1.BNKCOD = t2.DEPAR AND GRPACT = 'A'", 'left');
        $this->db->where("`GRPACT` = 'A' AND (`ACCNUM` LIKE '%0%' OR `ACCNUM` LIKE '%5%' OR `ACCNUM` LIKE '%9%' OR `ACCNUM` LIKE '%6%')  AND `ACCNUM` IS NOT NULL");
        $BANK = $this->db->get()->result_array();

//        echo $this->db->last_query();

//        echo '<pre>'; print_r($BANK);exit;
        foreach ($BANK as $key => $value){
            if($value['BNKCOD'] == 'K1'){
                $dividend_bank_id = "'006'";
            }else{
                $dividend_bank_id = 'NULL';
            }

            $member_id = sprintf("%06d",$value['MEM_NO']);
            $dividend_acc_num = $value['ACCNUM'];

            $this->db->select('t1.mem_apply_id');
            $this->db->from('coop_mem_apply as t1');
            $this->db->where(" t1.member_id = '".$member_id."'");
            $mem_apply_id = $this->db->get()->row_array()['mem_apply_id'];

            $dividend_acc_num = str_replace('\\',"",$dividend_acc_num);

            $sql = "INSERT INTO `coop_mem_bank_account`(`member_id`, `dividend_bank_id`, `dividend_acc_num`) VALUES ('$member_id', $dividend_bank_id, '$dividend_acc_num');";
            echo $sql.'<br>';


            $sql = "UPDATE `coop_mem_bank_account` SET `id_apply` = '$mem_apply_id' WHERE `member_id` = '$member_id' AND dividend_acc_num = '$dividend_acc_num';";
            echo $sql.'<br>';

        }



    }
}
