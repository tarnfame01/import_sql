<?php
//header("Content-Type: application/vnd.ms-excel"); // ประเภทของไฟล์
//header('Content-Disposition: attachment; filename="myexcel.xls"'); //กำหนดชื่อไฟล์
//header("Content-Type: application/force-download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Transfer-Encoding: binary");
//header("Content-Length: ".filesize("myexcel.xls"));

@readfile($filename);
set_time_limit (60);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('document/5spktcoop_average_dividend_fix.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
$sheetData = $objPHPExcel->setActiveSheetIndex(0);
$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
//echo $yeartitle."<br>";
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

$data = array();
$i=0;
foreach($sheetData as $key => $value){
    if($key >= 2){
//		if($key >= 2 && $key < 10){
        if($value['A'] == ''){
            break;
        }

        $data[$i]['member_id'] = trim($value['B']);
        $data[$i]['member_id'] = sprintf("%06d",$data[$i]['member_id']);
        $data[$i]['fullname'] = trim($value['C']);
        $data[$i]['fix_dividend'] = trim($value['D']);
        $i++;
    }
}

?>
<table border="1">
    <tr>
        <td>ลำดับ</td>
        <td>เลขทะเบียนสมาชิก</td>
        <td>ชื่อ-นามสกุล</td>
        <td>fix_ปันผล</td>
    </tr>
    <?php foreach ($data as $key => $value) {?>
        <tr>
            <td><?php echo $key + 1; ?></td>
            <td><?php echo $value['member_id']; ?></td>
            <td><?php echo $value['fullname']; ?></td>
            <td><?php echo number_format($value['fix_dividend'], 2); ?></td>
        </tr>
        <?php
    }
    ?>
</table>
<?php
$master_id = '6';
foreach($data as $key => $value){
    $date_now = date("Y-m-d h:i:s");
    $member_id = sprintf("%06d", $value['member_id']);
    $fix_dividend = $value['fix_dividend'];
    $sql_update = "INSERT INTO `coop_fix_dividend_average`(`master_id`, `member_id`, `dividend_value`, `average_return_value`, `gift_varchar`, `date_create`, `status`) VALUES ('".$master_id."', '".$member_id."', '".$fix_dividend."', 0, 0, '".$date_now."', '1');";
    echo $sql_update . '<br>';
}

?>