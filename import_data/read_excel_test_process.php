<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<?php 
set_time_limit (60);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
//$objPHPExcel = PHPExcel_IOFactory::load('document/test_process.xlsx');
$objPHPExcel = PHPExcel_IOFactory::load('document/test_share_loan_balance_2018_07_31.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
	$sheetData = $objPHPExcel->setActiveSheetIndex(0);
	$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
	//echo $yeartitle."<br>";
	$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
	//echo"<pre>";print_r($sheetData);exit;
	$data_date = '2018-07-31';
	$data = array();
	$i=0;
	$atm_count = 0;
	foreach($sheetData as $key => $value){
		if($key >= 4){
			$data[$i]['member_id'] = trim($value['A']);
			$data[$i]['firstname_th'] = trim($value['B']);
			$data[$i]['level_id'] = trim($value['C']);
			$data[$i]['share_period'] = trim($value['G']);
			$data[$i]['share_collect_value'] = trim($value['H']);
			
			$j=0;
			$data[$i]['loan'][$j]['period_now'] = trim($value['I']);
			$data[$i]['loan'][$j]['contract_number'] = trim($value['J']);
			$data[$i]['loan'][$j]['loan_amount_balance'] = trim($value['K']);
			$contract_code = explode('61',$data[$i]['loan'][$j]['contract_number']);
			$contract_code = explode('60',$contract_code[0]);
			$contract_code = explode('59',$contract_code[0]);
			$contract_code = explode('58',$contract_code[0]);
			if($contract_code[0] == 'ฉอ' || $contract_code[0] == 'ฉด'){
				$data[$i]['loan'][$j]['loan_type'] = 'atm';
				$atm_count++;
			}else{
				$sql = "SELECT type_id FROM coop_term_of_loan WHERE prefix_code = '".$contract_code[0]."."."'";
				$rs = $mysqli->query($sql);
				$row = $rs->fetch_assoc();
				$data[$i]['loan'][$j]['loan_type'] = $row['type_id'];
			}
			$j++;
			$data[$i]['loan'][$j]['period_now'] = trim($value['L']);
			$data[$i]['loan'][$j]['contract_number'] = trim($value['M']);
			$data[$i]['loan'][$j]['loan_amount_balance'] = trim($value['N']);
			$contract_code = explode('61',$data[$i]['loan'][$j]['contract_number']);
			$contract_code = explode('60',$contract_code[0]);
			$contract_code = explode('59',$contract_code[0]);
			$contract_code = explode('58',$contract_code[0]);
			$contract_code = explode('57',$contract_code[0]);
			$contract_code = explode('56',$contract_code[0]);
			$contract_code = explode('55',$contract_code[0]);
			if($contract_code[0] == '20' || $contract_code[0] == 'สบ'){
				$data[$i]['loan'][$j]['loan_type'] = '12';
				$data[$i]['loan'][$j]['guarantee_type'] = '1';
			}else if($contract_code[0] == '22' || $contract_code[0] == 'สห'){
				$data[$i]['loan'][$j]['loan_type'] = '12';
				$data[$i]['loan'][$j]['guarantee_type'] = '2';
			}else{
				$data[$i]['loan'][$j]['loan_type'] = '12';
			}
			
			$j++;
			$data[$i]['loan'][$j]['period_now'] = trim($value['O']);
			$data[$i]['loan'][$j]['contract_number'] = trim($value['P']);
			$data[$i]['loan'][$j]['loan_amount_balance'] = trim($value['Q']);
			$j++;
			
			$i++;
		}
	}
	//echo $atm_count."<br>";
	//echo"<pre>";print_r($data);exit;
	foreach($data as $key => $value){
			$sql_level_id = "SELECT 
				level.id as level_id,
				faction.id as faction_id,
				department.id as department_id
			FROM 
				coop_mem_group as level
				LEFT JOIN coop_mem_group as faction ON level.mem_group_parent_id = faction.id
				LEFT JOIN coop_mem_group as department ON faction.mem_group_parent_id = department.id
			WHERE 
			level.mem_group_id = '".$value['level_id']."'";
			$rs_level_id = $mysqli->query($sql_level_id);
			$row_level_id = $rs_level_id->fetch_assoc();
			
			$sql_mem_apply_id = "SELECT mem_apply_id FROM coop_mem_apply WHERE mem_apply_id LIKE '".date("Ym")."%' ORDER BY mem_apply_id DESC limit 1";
			$rs_mem_apply_id = $mysqli->query($sql_mem_apply_id);
			$row_mem_apply_id = $rs_mem_apply_id->fetch_assoc();
			if(!empty($row_mem_apply_id)) {
				$id = (int)substr($row_mem_apply_id["mem_apply_id"], 6);
				$mem_apply_id = date("Ym").sprintf("%06d", $id + 1);
			}else {
				$mem_apply_id = date("Ym")."000001";
			}
			$sql = "SELECT * FROM coop_mem_apply WHERE member_id = '".$value['member_id']."'";
			$rs = $mysqli->query($sql);
			$row = $rs->fetch_assoc();
			if(empty($row)){
				$sql_insert = "INSERT INTO coop_mem_apply SET
					member_id = '".$value['member_id']."',
					apply_type_id = '1',
					mem_apply_id = '".$mem_apply_id."',
					mem_type = '1',
					member_status = '1',
					firstname_th = '".$value['firstname_th']."',
					level = '".$row_level_id['level_id']."',
					faction = '".$row_level_id['faction_id']."',
					department = '".$row_level_id['department_id']."'
				";
				if(@$_GET['runscript']=='runscript'){
					$mysqli->query($sql_insert);
				}
				echo $sql_insert."<br>";
			}
			
			$sql_insert = "INSERT INTO coop_mem_share SET 
				member_id = '".$value['member_id']."',
				admin_id = '1',
				share_type = 'SPM',
				share_date = '".$data_date."',
				share_payable = '0',
				share_payable_value = '0',
				share_early = '".($value['share_collect_value']/10)."',
				share_early_value = '".$value['share_collect_value']."',
				share_collect = '".($value['share_collect_value']/10)."',
				share_collect_value = '".$value['share_collect_value']."',
				share_value = '10',
				share_status = '1',
				share_bill = 'N/A',
				share_bill_date = '".$data_date."',
				share_period = '".$value['share_period']."'
			";
			if(@$_GET['runscript']=='runscript'){
				$mysqli->query($sql_insert);
			}
			echo $sql_insert."<br>";
			echo "-----<br>";
			foreach($value['loan'] as $key_loan => $value_loan){
				if($value_loan['contract_number']!=''){
					if($value_loan['loan_type'] != 'atm'){
						$sql = "SELECT petition_number FROM coop_loan ORDER BY id DESC limit 1";
						$rs = $mysqli->query($sql);
						$row = $rs->fetch_assoc();
						$petition_number = (int)@$row['petition_number']+1;
						$petition_number = sprintf('% 06d',@$petition_number);
						
						$sql = "SELECT interest_rate FROM coop_term_of_loan WHERE type_id = '".$value_loan['loan_type']."'";
						$rs = $mysqli->query($sql);
						$row = $rs->fetch_assoc();
						$interest_rate = $row['interest_rate'];
						
						$sql_insert = "INSERT INTO coop_loan SET
							petition_number = '".$petition_number."',
							member_id = '".$value['member_id']."',
							contract_number = '".$value_loan['contract_number']."',
							loan_type = '".$value_loan['loan_type']."',
							period_now = '".$value_loan['period_now']."',
							loan_amount = '".$value_loan['loan_amount_balance']."',
							loan_amount_balance = '".$value_loan['loan_amount_balance']."',
							interest_per_year = '".$interest_rate."',
							createdatetime = '".$data_date."',
							period_type = '1',
							pay_type = '1',
							admin_id = '1'
						";
						if(@$_GET['runscript']=='runscript'){
							$mysqli->query($sql_insert);
							$loan_id = $mysqli->insert_id;
						}else{
							$loan_id = 'test';
						}
						echo $sql_insert."<br>";
						
						$sql_insert = "INSERT INTO coop_loan_transaction SET 
							loan_id = '".$loan_id."',
							loan_amount_balance = '".$value_loan['loan_amount_balance']."',
							transaction_datetime = '".$data_date."'
						";
						if(@$_GET['runscript']=='runscript'){
							$mysqli->query($sql_insert);
						}
						echo $sql_insert."<br>";
						
						$sql_insert = "INSERT INTO coop_loan_transfer SET
							loan_id = '".$loan_id."',
							date_transfer = '".$data_date."',
							transfer_status = '0',
							admin_id = '1'
						";
						if(@$_GET['runscript']=='runscript'){
							$mysqli->query($sql_insert);
						}
						echo $sql_insert."<br>";
					}else{
						$sql = "SELECT * FROM coop_loan_atm_setting";
						$rs = $mysqli->query($sql);
						$row_setting = $rs->fetch_assoc();
						
						$sql = "SELECT petition_number FROM coop_loan_atm ORDER BY loan_atm_id DESC limit 1";
						$rs = $mysqli->query($sql);
						$row = $rs->fetch_assoc();
						$petition_number = (int)@$row['petition_number']+1;
						$petition_number = sprintf('% 06d',@$petition_number);
						
						$loan_amount_balance = 150000 - $value_loan['loan_amount_balance'];
						
						$sql_insert = "INSERT INTO coop_loan_atm SET
							petition_number = '".$petition_number."',
							member_id = '".$value['member_id']."',
							contract_number = '".$value_loan['contract_number']."',
							total_amount = '150000',
							total_amount_balance = '".$loan_amount_balance."',
							total_amount_approve = '150000',
							loan_atm_status = '1',
							createdatetime = '".$data_date."',
							approve_date = '".$data_date."',
							admin_id = '1'
						";
						if(@$_GET['runscript']=='runscript'){
							$mysqli->query($sql_insert);
							$loan_atm_id = $mysqli->insert_id;
						}else{
							$loan_atm_id = 'test';
						}
						echo $sql_insert."<br>";
						
						$sql = "SELECT petition_number FROM coop_loan_atm_detail ORDER BY loan_id DESC limit 1";
						$rs = $mysqli->query($sql);
						$row = $rs->fetch_assoc();
						$petition_number = (int)@$row['petition_number']+1;
						$petition_number = sprintf('% 06d',@$petition_number);
						
						$sql_insert = "INSERT INTO coop_loan_atm_detail SET
							loan_atm_id = '".$loan_atm_id."',
							member_id = '".$value['member_id']."',
							loan_amount = '".$value_loan['loan_amount_balance']."',
							loan_amount_balance = '".$value_loan['loan_amount_balance']."',
							loan_date = '".$data_date."',
							petition_number = '".$petition_number."',
							loan_status = '0',
							loan_description = 'ทำรายการกู้ ATM',
							date_start_period = '".$data_date."',
							transfer_status = '1',
							principal_per_month = '".ceil($value_loan['loan_amount_balance']/24)."'
						";
						if(@$_GET['runscript']=='runscript'){
							$mysqli->query($sql_insert);
						}
						echo $sql_insert."<br>";
						$sql_insert = "INSERT INTO coop_loan_atm_transaction SET 
							loan_atm_id = '".$loan_atm_id."',
							loan_amount_balance = '".$value_loan['loan_amount_balance']."',
							transaction_datetime = '".$data_date."'
						";
						if(@$_GET['runscript']=='runscript'){
							$mysqli->query($sql_insert);
						}
						echo $sql_insert."<br>";
					}
					echo "-----<br>";
				}
			}
		echo "_______________________________<br>";
	}
?>