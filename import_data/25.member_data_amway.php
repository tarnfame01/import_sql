<?php
//header("Content-Type: application/vnd.ms-excel"); // ประเภทของไฟล์
//header('Content-Disposition: attachment; filename="myexcel.xls"'); //กำหนดชื่อไฟล์
//header("Content-Type: application/force-download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Transfer-Encoding: binary");
//header("Content-Length: ".filesize("myexcel.xls"));

@readfile($filename);
set_time_limit (60);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('document/18member_data_amway.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
$sheetData = $objPHPExcel->setActiveSheetIndex(0);
$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
//echo $yeartitle."<br>";
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

$data = array();
$i=0;
//echo '<pre>'; print_r($sheetData);exit;
foreach($sheetData as $key => $value){
    if($key >= 2){
//		if($key >= 18 && $key <= 20){
        if($value['A'] == ''){
            break;
        }

        $data[$i]['member_id'] = sprintf("%06d", trim($value['A']));;
        $data[$i]['employee_id'] = trim($value['B']);
        $data[$i]['full_name'] = trim($value['C']);
        $data[$i]['id_card'] = trim($value['D']);
        $data[$i]['birthday'] = trim($value['E']);
        $data[$i]['apply_date'] = trim($value['F']);
        $data[$i]['address_no'] = trim($value['G']);
        $data[$i]['address_moo'] = trim($value['H']);
//        $data[$i]['address_village'] = trim($value['D']);
        $data[$i]['address_road'] = trim($value['I']);
        $data[$i]['address_soi'] = trim($value['J']);
        $data[$i]['district'] = trim($value['K']);
        $data[$i]['amphur'] = trim($value['L']);
        $data[$i]['province'] = trim($value['M']);
        $data[$i]['zipcode'] = trim($value['N']);
        $data[$i]['moblie'] = trim($value['O']);
        $i++;
    }
}
$datas = array();
foreach ($data as $key => $value){
//    echo '<pre>'; print_r($value);exit;
    $value['district'] = str_replace('โสนสวย',"หนองโสน",$value['district']);
    $sql_district = "SELECT count(*) as count_district, district_id FROM `amwaycoop_sys`.`coop_district` WHERE `district_name` LIKE '%{$value['district']}%'";
    $rs_district = $mysqli->query($sql_district);
    $district_id = '';
    while($row_district = $rs_district->fetch_assoc()){
        $district_name = '';
        if(!empty($value['district'])){
            if($row_district['count_district'] > 1){
                $sql_district2 = "SELECT district_name FROM `amwaycoop_sys`.`coop_district` WHERE `district_name` LIKE '%{$value['district']}%'";
                $rs_district2 = $mysqli->query($sql_district2);
                $num_district_name = 0;
                while($row_district2 = $rs_district2->fetch_assoc()){
                    $row_district2['district_name'] = str_replace(' ',"",$row_district2['district_name']);
                    if($row_district2['district_name'] == $value['district']){
//                        echo $row_district2['district_name'].'<br>';
                        $district_name = $row_district2['district_name'];
                        $num_district_name++;
                    }
                }
                if($num_district_name > 1){
                    $sql_amphur = "SELECT count(*) as count_amphur, amphur_id FROM `amwaycoop_sys`.`coop_amphur` WHERE `amphur_name` LIKE '%{$value['amphur']}%'";
                    $rs_amphur = $mysqli->query($sql_amphur);
                    $amphur_id = '';
                    while($row_amphur = $rs_amphur->fetch_assoc()){
                        if($row_amphur['count_amphur'] == 1){
                            $amphur_id = $row_amphur['amphur_id'];
                        }else{
                            $sql_province = "SELECT count(*) as count_province, province_id FROM `amwaycoop_sys`.`coop_province` WHERE `province_name` LIKE '%{$value['province']}%'";
                            $rs_province = $mysqli->query($sql_province);
                            $province_id = '';
                            while($row_province = $rs_province->fetch_assoc()){
                                if($row_province['count_province'] == 1){
                                    $province_id = $row_province['province_id'];
                                }
                            }

                            if(!empty($province_id)){
                                $sql_amphur = "SELECT count(*) as count_amphur, amphur_id FROM `amwaycoop_sys`.`coop_amphur` WHERE `amphur_name` LIKE '%{$value['amphur']}%' AND province_id = '{$province_id}'";
                                $rs_amphur = $mysqli->query($sql_amphur);
                                while($row_amphur = $rs_amphur->fetch_assoc()){
                                    if($row_amphur['count_amphur'] == '1'){
                                        $amphur_id = $row_amphur['amphur_id'];
                                    }else{
                                        $sql_amphur = "SELECT count(*) as count_amphur, amphur_id FROM `amwaycoop_sys`.`coop_amphur` WHERE `amphur_name` LIKE '%{$value['amphur']}%' AND province_id = '{$province_id}' AND `amphur_name` NOT LIKE '%*%' ";
                                        $rs_amphur = $mysqli->query($sql_amphur);
                                        $amphur_id = '';
                                        while($row_amphur = $rs_amphur->fetch_assoc()){
                                            if($row_amphur['count_amphur'] == '1'){
                                                $amphur_id = $row_amphur['amphur_id'];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if(!empty($amphur_id)){
                        $sql_district = "SELECT count(*) as count_district, district_id FROM `amwaycoop_sys`.`coop_district` WHERE `district_name` LIKE '%{$value['district']}%' AND amphur_id = '{$amphur_id}' ";
                        $rs_district = $mysqli->query($sql_district);
                        while($row_district_out = $rs_district->fetch_assoc()){
                            if($row_district_out['count_district'] == 1){
                                $district_id = $row_district_out['district_id'];
                            }else{
                                'เพิ่มเงื่อนไขหาอำเภอที่ไม่มี *'.'<br>';
                            }
                        }
                    }
                }else if($num_district_name == 1){
                    $district_id = $row_district['district_id'];
                }else{
                    'หาไม่ได้'.'<br>';
                }
            }else{
                $district_id = $row_district['district_id'];
            }
//            echo $value['member_id'].' '.$value['district'].' '.$district_id.'<br>';
//            echo '<hr>';
        }
    }

    if(!empty($district_id)){
        $sql_district = "SELECT district_id, amphur_id, province_id FROM `amwaycoop_sys`.`coop_district` WHERE `district_id` = '{$district_id}' ";
        $rs_district = $mysqli->query($sql_district);
        while($row_district = $rs_district->fetch_assoc()){
//            echo '<pre>'; print_r($row_district);exit;
            $district_id = "'{$row_district['district_id']}'";
            $amphur_id = "'{$row_district['amphur_id']}'";
            $province_id = "'{$row_district['province_id']}'";
        }
    }else{
        $district_id = "NULL";
        $amphur_id = "NULL";
        $province_id =  "NULL";
    }

    $sql = "UPDATE `coop_mem_apply` SET `id_card` = '{$value['id_card']}', `address_no` = '{$value['address_no']}', `address_moo` = '{$value['address_moo']}', `address_road` = '{$value['address_road']}', `address_soi` = '{$value['address_soi']}', `province_id` = {$province_id}, `amphur_id` = {$amphur_id}, `district_id` = {$district_id}, `zipcode` = '{$value['zipcode']}', `mobile` = '{$value['moblie']}', `employee_id` = '{$value['employee_id']}' WHERE `member_id` = '{$value['member_id']}';";

    echo $sql.'<br>';

}
//echo '<pre>'; print_r($datas);exit;


