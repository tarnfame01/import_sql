<?php 
set_time_limit (60);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('document/data_member_glocoop.xls');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
	$sheetData = $objPHPExcel->setActiveSheetIndex(0);
	$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
	//echo $yeartitle."<br>";
	$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
	
	$data = array();
	$i=0;
	foreach($sheetData as $key => $value){		
		if($key >= 3){
		//if($key >= 3 && $key <= 20){
			if($value['A'] == ''){
				break;
			}

			$data[$i]['member_id'] = trim($value['B']);
			$data[$i]['share_period'] = trim($value['H']); 
			$data[$i]['share_payable_value'] = trim($value['I']);
			$data[$i]['share_early_value'] = trim($value['J']) - trim($value['I']);//หุ้นก่อนหน้า (อันนี้เพิ่มเติมเอง)
			$data[$i]['share_collect_value'] = trim($value['J']);
			$data[$i]['share_date'] = '2019-03-31 00:00:00';
			$data[$i]['share_value'] = '10';
			$data[$i]['admin_id'] = '1';
			$data[$i]['share_type'] = 'SPM';
			$i++;
		}
	}
	
	/*echo '<pre>'; print_r($data); echo '</pre>';	
	echo '<table border= "1" width="90%">';
		echo '<tr>';
			echo '<td>member_id</td>';
			echo '<td>งวดส่งหุ้น</td>';
			echo '<td>หุ้น/เดือน</td>';
			echo '<td>หุ้นก่อนหน้า (อันนี้เพิ่มเติมเอง)</td>';
			echo '<td>หุ้นสะสม</td>';
		echo '</tr>';
		*/
	
	foreach($data as $key => $value){		
		$sql_insert= "INSERT coop_mem_share SET 
			member_id = '".$value['member_id']."',
			admin_id = '".$value['admin_id']."',
			share_type = '".$value['share_type']."',
			share_date = '".$value['share_date']."',
			share_status = '1',
			share_payable = '".($value['share_payable_value']/10)."',
			share_payable_value = '".$value['share_payable_value']."',
			share_early = '".($value['share_early_value']/10)."',
			share_early_value = '".$value['share_early_value']."',
			share_collect = '".($value['share_collect_value']/10)."',
			share_collect_value = '".$value['share_collect_value']."',
			share_value = '".$value['share_value']."',
			share_period = '".$value['share_period']."'
			;";
			echo $sql_insert.'<br>';
	}
	//echo '</table>';
	//echo "<pre>"; print_r($data);
	exit;
?>