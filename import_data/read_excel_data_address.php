<?php 
set_time_limit (60);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('document/data_member_glocoop.xls');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
	$sheetData = $objPHPExcel->setActiveSheetIndex(0);
	$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
	//echo $yeartitle."<br>";
	$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
	
	$data = array();
	$i=0;
	foreach($sheetData as $key => $value){		
		if($key >= 3){
		//if($key >= 3 && $key <= 20){
			if($value['A'] == ''){
				break;
			}

			$data[$i]['member_id'] = trim($value['B']);
			$data[$i]['address'] = trim($value['O']);
			$i++;
		}
	}
	$address_b = '';
	$address_no = '';
	$address_moo = '';
	$address_village = '';
	$address_road = '';
	$address_soi = '';
	$district_id = '';
	$amphur_id = '';
	$province_id = '';
	$zipcode = '';
	
	$sql_province = "SELECT province_name,province_id FROM coop_province";
	$rs_province = $mysqli->query($sql_province);
	$arr_province_list= array();
	$arr_amphur_list = array();
	$arr_district_list = array();
	$arr_zipcode_list = array();
	while($row_province = $rs_province->fetch_assoc()){
		$province_name = trim($row_province['province_name']);
		$province_id = trim($row_province['province_id']);
		$arr_province_list[$province_name] = $province_id;
		
		$sql_amphur = "SELECT amphur_name,amphur_id,province_id FROM coop_amphur WHERE province_id = '".$province_id."'";
		$rs_amphur = $mysqli->query($sql_amphur);
		while($row_amphur= $rs_amphur->fetch_assoc()){
			if($province_id == '1'){
				$amphur_name = str_replace("เขต","",trim($row_amphur['amphur_name']));
			}else{			
				$amphur_name = trim($row_amphur['amphur_name']);
			}
			$amphur_id = trim($row_amphur['amphur_id']);
			$arr_amphur_list[$province_id][$amphur_name] = $row_amphur['amphur_id'];
			
			$sql_district = "SELECT t1.district_code,t1.district_name,t1.district_id,t1.amphur_id,t1.province_id,t2.zipcode FROM coop_district AS t1
							LEFT JOIN coop_zipcode AS t2 ON t1.district_code = t2.district_code
							WHERE t1.province_id = '".$province_id."' AND t1.amphur_id = '".$amphur_id."'";
			$rs_district = $mysqli->query($sql_district);
			while($row_district= $rs_district->fetch_assoc()){
				$district_id = trim($row_district['district_id']);
				$district_name = trim($row_district['district_name']);
				$arr_district_list[$province_id][$amphur_id][$district_name] = $row_district['district_id'];
				$arr_zipcode_list[$province_id][$amphur_id][$district_id] = $row_district['zipcode'];
			}
		}
	}	
	
	$u_province_id = '';
	$u_amphur = '';
	$u_district = '';
	$u_zipcode = '';
	//echo '<pre>'; print_r($arr_amphur); echo '</pre>';	
	/*echo '<table border= "1" width="90%">';
		echo '<tr>';
			echo '<td>address</td>';
			echo '<td>address_no</td>';
			echo '<td>address_moo</td>';
			echo '<td>address_village</td>';
			echo '<td>address_road</td>';
			echo '<td>address_soi</td>';
			echo '<td>district_id</td>';
			echo '<td>district_id</td>';
			echo '<td>amphur_id</td>';
			echo '<td>amphur_id</td>';
			echo '<td>province_id</td>';
			echo '<td>province_id</td>';
			echo '<td>zipcode</td>';
		echo '</tr>';
	*/	
	foreach($data as $key => $value){		
		//echo '<pre>'; print_r($value['address']); echo '</pre>';		
		$address_b = $value['address'];
		$arr_amphur = array();
		$text_amphur = '';
		if (strpos($address_b, 'เขต') !== false) {
			$arr_amphur = explode('เขต',$address_b);
			$p_amphur = 'เขต';
			$text_amphur = $p_amphur.$arr_amphur[1];
		}
		if (strpos($address_b, 'อ.') !== false) {
			$arr_amphur = explode('อ.',$address_b);
			$p_amphur = 'อ.';
			$text_amphur = $p_amphur.$arr_amphur[1];
		}
		if (strpos($address_b, 'อำเภอ') !== false) {
			$arr_amphur = explode('อำเภอ',$address_b);
			$p_amphur = 'อำเภอ';
			$text_amphur = $p_amphur.$arr_amphur[1];
		}
		
		
		$amphur = explode(' ',$arr_amphur[1]);
		
		$amphur_id = $amphur[0];
		$province_n = $amphur[1].' '.$amphur[2].' '.$amphur[3];		
		
		preg_match("/[[:digit:]]+\.?[[:digit:]]*/", $province_n , $arr_zipcode);
		$zipcode = $arr_zipcode[0];		
		$province_n = str_replace("จ.","",$province_n);
		$province_id = str_replace($arr_zipcode[0],"",$province_n);
		
		//echo '<pre>'; print_r($arr_soi); echo '</pre>';
		$address_b = str_replace($text_amphur,"",$address_b);
		$arr_district = array();
		$text_district = '';
		if (strpos($address_b, 'แขวง') !== false) {
			$arr_district = explode('แขวง',$address_b);
			$p_district = 'แขวง';
			$text_district = $p_district.$arr_district[1];
		}
		
		if (strpos($address_b, 'แถวง') !== false) {
			$arr_district = explode('แถวง',$address_b);
			$p_district = 'แถวง';
			$text_district = $p_district.$arr_district[1];
		}
		
		if (strpos($address_b, 'ต.') !== false) {
			$arr_district = explode('ต.',$address_b);
			$p_district = 'ต.';
			$text_district = $p_district.$arr_district[1];
		}
		$district_id = $arr_district[1];
		
	
		$address_b = str_replace($text_district,"",$address_b);
		$arr_road = array();
		$text_road = '';
		if (strpos($address_b, 'ถ.') !== false) {
			$p_road = 'ถ.';
			$arr_road = explode('ถ.',$address_b);
			$text_road = $p_road.$arr_road[1];
		}
		if (strpos($address_b, 'ถนน') !== false) {
			$p_road = 'ถนน';
			$arr_road = explode('ถนน',$address_b);
			$text_road = $p_road.$arr_road[1];
		}
		$address_road = ($arr_road[1] != '')?'ถนน'.$arr_road[1]:'';
		
		$address_b = str_replace($text_road,"",$address_b);
		$arr_soi = array();
		$text_soi = '';
		if (strpos($address_b, 'ซ.') !== false) {			
			$arr_soi = explode('ซ.',$address_b);
			$p_soi = 'ซ.';
			$text_soi = $p_soi.$arr_soi[1];
		}
		if (strpos($address_b, 'ซอย') !== false) {			
			$arr_soi = explode('ซอย',$address_b);
			$p_soi = 'ซอย';
			$text_soi = $p_soi.$arr_soi[1];
		}		 
		$address_soi = ($arr_soi[1] != '')?'ซอย'.$arr_soi[1]:'';
		$address_b = str_replace($text_soi,"",$address_b);
		
		$arr_village = array();
		$text_village = '';
		if (strpos($address_b, 'หมู่บ้าน') !== false) {			
			$arr_village = explode('หมู่บ้าน',$address_b);
			$p_village = 'หมู่บ้าน';
			$text_village = $p_village.$arr_village[1];
		}	 
		$address_village = ($arr_village[1] != '')?'หมู่บ้าน'.$arr_village[1]:'';
		$address_b = str_replace($text_village,"",$address_b);
		
		$arr_moo = array();
		$text_moo = '';
		if (strpos($address_b, 'หมู่') !== false) {			
			$arr_moo = explode('หมู่',$address_b);
			$p_moo = 'หมู่';
			$text_moo = $p_moo.$arr_moo[1];
		}

		if (strpos($address_b, 'ม.') !== false) {			
			$arr_moo = explode('ม.',$address_b);
			$p_moo = 'ม.';
			$text_moo = $p_moo.$arr_moo[1];
		}	
		$address_moo = ($arr_moo[1] != '')?'หมู่ '.$arr_moo[1]:'';
		$address_b = str_replace($text_moo,"",$address_b);
		$address_no = $address_b;
		
		//echo 'address_moo='.$address_moo.'<br>';
		//echo 'text_moo='.$text_moo.'<br>';		
		//echo 'address_b='.$address_b.'<br>';
		//echo '<hr>';
		
		
		$data[$key]['address_no'] = $address_no;
		$data[$key]['address_moo'] = $address_moo;
		$data[$key]['address_village'] = $address_village;
		$data[$key]['address_road'] = $address_road;
		$data[$key]['address_soi'] = $address_soi;
		$data[$key]['district_id'] = trim($district_id);
		$data[$key]['amphur_id'] = trim($amphur_id);
		//กรุงเทพมหานคร
		if (strpos($province_id, 'กรุงเทพฯ') !== false) {
			$province_id = str_replace("กรุงเทพฯ","กรุงเทพมหานคร",$province_id);
		}
		if (strpos($province_id, 'กทม.') !== false) {
			$province_id = str_replace("กทม.","กรุงเทพมหานคร",$province_id);
		}
		if (strpos($province_id, 'กทม') !== false) {
			$province_id = str_replace("กทม","กรุงเทพมหานคร",$province_id);
		}
		$data[$key]['province_id'] = trim($province_id);
		$data[$key]['zipcode'] = $zipcode;
		
		/*$sql_province = "SELECT province_name,province_id FROM coop_province WHERE province_name LIKE '".$data[$key]['province_id']."%'";
		$rs_province = $mysqli->query($sql_province);
		$arr_province = array();
		$row_province = $rs_province->fetch_assoc();
		$u_province_id = $row_province['province_id'];
		*/
		//$u_province_id = '';
		$u_province_id = $arr_province_list[$data[$key]['province_id']];
		$u_amphur = $arr_amphur_list[$u_province_id][$data[$key]['amphur_id']];
		$u_district = $arr_district_list[$u_province_id][$u_amphur][$data[$key]['district_id']];
		$u_zipcode = $arr_zipcode_list[$u_province_id][$u_amphur][$u_district];
	
		/*echo '<tr>';
			echo '<td>'.$value['address'].'</td>';
			echo '<td>'.(($data[$key]['address_no'] != '')?$data[$key]['address_no']:'&nbsp;').'</td>';
			echo '<td>'.(($data[$key]['address_moo'] != '')?$data[$key]['address_moo']:'&nbsp;').'</td>';
			echo '<td>'.(($data[$key]['address_village'] != '')?$data[$key]['address_village']:'&nbsp;').'</td>';
			echo '<td>'.(($data[$key]['address_road'] != '')?$data[$key]['address_road']:'&nbsp;').'</td>';
			echo '<td>'.(($data[$key]['address_soi'] != '')?$data[$key]['address_soi']:'&nbsp;').'</td>';
			echo '<td>'.(($data[$key]['district_id'] != '')?$data[$key]['district_id']:'&nbsp;').'</td>';
			echo '<td>'.(($data[$key]['district_id'] != '')?$u_district:'&nbsp;').'</td>';
			echo '<td>'.(($data[$key]['amphur_id'] != '')?$data[$key]['amphur_id']:'&nbsp;').'</td>';
			echo '<td>'.(($data[$key]['amphur_id'] != '')?$u_amphur:'&nbsp;').'</td>';
			echo '<td>'.(($data[$key]['province_id'] != '')?$data[$key]['province_id']:'&nbsp;').'</td>';
			echo '<td>'.(($data[$key]['province_id'] != '')?$u_province_id:'&nbsp;').'</td>';
			echo '<td>'.(($data[$key]['zipcode'] != '')?$data[$key]['zipcode']:'&nbsp;').'</td>';			
			echo '<td>'.(($data[$key]['zipcode'] != '')?$u_zipcode:'&nbsp;').'</td>';			
		echo '</tr>';
		*/
		
		$sql_update= "UPDATE coop_mem_apply SET 
			address_no = '".$data[$key]['address_no']."',
			address_moo = '".$data[$key]['address_moo']."',
			address_village = '".$data[$key]['address_village']."',
			address_road = '".$data[$key]['address_road']."',
			address_soi = '".$data[$key]['address_soi']."',
			province_id = '".$u_district."',
			amphur_id = '".$u_amphur."',
			district_id = '".$u_province_id."',
			zipcode = '".$u_zipcode."'
			WHERE member_id = '".$value['member_id']."' ;";
		echo $sql_update.'<br>';	
	}
	//echo '</table>';
	//echo "<pre>"; print_r($data);
	exit;
?>