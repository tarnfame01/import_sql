<?php
//header("Content-Type: application/vnd.ms-excel"); // ประเภทของไฟล์
//header('Content-Disposition: attachment; filename="myexcel.xls"'); //กำหนดชื่อไฟล์
//header("Content-Type: application/force-download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Transfer-Encoding: binary");
//header("Content-Length: ".filesize("myexcel.xls"));

@readfile($filename);
set_time_limit (60);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('document/spktcoop_average_dividend.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
	$sheetData = $objPHPExcel->setActiveSheetIndex(0);
	$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
	//echo $yeartitle."<br>";
	$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
	
	$data = array();
	$i=0;
	foreach($sheetData as $key => $value){		
		if($key >= 5){
//		if($key >= 5 && $key < 105){
			if($value['A'] == ''){
				break;
			}

			$data[$i]['member_id'] = trim($value['A']);
			$data[$i]['fullname'] = trim($value['B']);
			$data[$i]['dividend'] = trim($value['E']);
			$data[$i]['average'] = trim($value['F']);
			$data[$i]['dividend_average'] = trim($value['G']);
			$data[$i]['deduct_1'] = trim($value['H']);
			$data[$i]['deduct_2'] = trim($value['I']);
			$data[$i]['deduct_3'] = trim($value['J']);
			$data[$i]['deduct_4'] = trim($value['K']);
			$data[$i]['deduct_5'] = trim($value['L']);
			$data[$i]['deduct_6'] = trim($value['M']);
			$data[$i]['deduct_8'] = trim($value['N']);
			$data[$i]['deduct_7'] = trim($value['O']);
			$i++;
		}
	}
?>
<!--<table border="1">-->
<!--    <tr>-->
<!--        <td>ลำดับ</td>-->
<!--        <td>เลขทะเบียนสมาชิก</td>-->
<!--        <td>ชื่อ-นามสกุล</td>-->
<!--        <td>ปันผล</td>-->
<!--        <td>เฉลี่ยคืน</td>-->
<!--        <td>รวม</td>-->
<!--    </tr>-->
<!--    --><?php //foreach ($data as $key => $value) { ?>

<!--    <tr>-->
<!--        <td>--><?php //echo $key+1;?><!--</td>-->
<!--        <td>--><?php //echo $value['member_id'];?><!--</td>-->
<!--        <td>--><?php //echo $value['fullname'];?><!--</td>-->
<!--        <td>--><?php //echo number_format($value['dividend'], 2);?><!--</td>-->
<!--        <td>--><?php //echo number_format($value['average'], 2);?><!--</td>-->
<!--        <td>--><?php //echo number_format($value['dividend_average'], 2);?><!--</td>-->
<!--    </tr>-->
<?php
//    }
    ?>
<!--</table>-->
<?php
$date_now = date("Y/m/d h:i:s", strtotime('+6 hour'));
$master_id = '6';
foreach($data as $key => $value){
    $member_id = sprintf("%06d",$value['member_id']);
    $sql_update= "INSERT coop_dividend_average SET
        member_id = '".$member_id."',
        master_id = '".$master_id."',
        year = '2020',
        dividend_value = '".sprintf("%.2f", $data[$key]['dividend'])."',
        average_return_value = '".sprintf("%.2f", $data[$key]['average'])."',
        gift_varchar = '0',
        dividend_percent = '4.4',
        average_percent = '10',
        date_create = '".$date_now."';";
    echo $sql_update.'<br>';
}

exit;
foreach($data as $key => $value){
    $member_id = sprintf("%06d",$value['member_id']);
    if(!empty($data[$key]['deduct_1']) && $data[$key]['deduct_1'] != '-' && $data[$key]['deduct_1'] != '0'){
        $sql_update= "INSERT coop_dividend_deduct SET
        member_id = '".$member_id."',
        master_id = '".$master_id."',
        deduct_id = '1',
        amount = '".$data[$key]['deduct_1']."',
        date_create = '".$date_now."',
        update_time = '".$date_now."';";
        echo $sql_update.'<br>';
    }
    if(!empty($data[$key]['deduct_2']) && $data[$key]['deduct_2'] != '-'  && $data[$key]['deduct_2'] != '0'){
        $sql_update= "INSERT coop_dividend_deduct SET
        member_id = '".$member_id."',
        master_id = '".$master_id."',
        deduct_id = '2',
        amount = '".$data[$key]['deduct_2']."',
        date_create = '".$date_now."',
        update_time = '".$date_now."';";
        echo $sql_update.'<br>';
    }
    if(!empty($data[$key]['deduct_3']) && $data[$key]['deduct_3'] != '-'  && $data[$key]['deduct_3'] != '0' ){
        $sql_update= "INSERT coop_dividend_deduct SET
        member_id = '".$member_id."',
        master_id = '".$master_id."',
        deduct_id = '3',
        amount = '".$data[$key]['deduct_3']."',
        date_create = '".$date_now."',
        update_time = '".$date_now."';";
        echo $sql_update.'<br>';
    }
    if(!empty($data[$key]['deduct_4']) && $data[$key]['deduct_4'] != '-'  && $data[$key]['deduct_4'] != '0' ){
        $sql_update= "INSERT coop_dividend_deduct SET
        member_id = '".$member_id."',
        master_id = '".$master_id."',
        deduct_id = '4',
        amount = '".$data[$key]['deduct_4']."',
        date_create = '".$date_now."',
        update_time = '".$date_now."';";
        echo $sql_update.'<br>';
    }
    if(!empty($data[$key]['deduct_5']) && $data[$key]['deduct_5'] != '-'  && $data[$key]['deduct_5'] != '0' ){
        $sql_update= "INSERT coop_dividend_deduct SET
        member_id = '".$member_id."',
        master_id = '".$master_id."',
        deduct_id = '5',
        amount = '".$data[$key]['deduct_5']."',
        date_create = '".$date_now."',
        update_time = '".$date_now."';";
        echo $sql_update.'<br>';
    }
    if(!empty($data[$key]['deduct_6']) && $data[$key]['deduct_6'] != '-'  && $data[$key]['deduct_6'] != '0' ){
        $sql_update= "INSERT coop_dividend_deduct SET
        member_id = '".$member_id."',
        master_id = '".$master_id."',
        deduct_id = '6',
        amount = '".$data[$key]['deduct_6']."',
        date_create = '".$date_now."',
        update_time = '".$date_now."';";
        echo $sql_update.'<br>';
    }
    if(!empty($data[$key]['deduct_7']) && $data[$key]['deduct_7'] != '-' && $data[$key]['deduct_7'] != '0' ){
        $sql_update= "INSERT coop_dividend_deduct SET
        member_id = '".$member_id."',
        master_id = '".$master_id."',
        deduct_id = '7',
        amount = '".$data[$key]['deduct_7']."',
        date_create = '".$date_now."',
        update_time = '".$date_now."';";
        echo $sql_update.'<br>';
    }
    if(!empty($data[$key]['deduct_8']) && $data[$key]['deduct_8'] != '-' && $data[$key]['deduct_8'] != '0' ){
        $sql_update= "INSERT coop_dividend_deduct SET
        member_id = '".$member_id."',
        master_id = '".$master_id."',
        deduct_id = '8',
        amount = '".$data[$key]['deduct_8']."',
        date_create = '".$date_now."',
        update_time = '".$date_now."';";
        echo $sql_update.'<br>';
    }
}
?>

