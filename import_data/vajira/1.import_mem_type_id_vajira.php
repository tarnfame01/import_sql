<?php
//header("Content-Type: application/vnd.ms-excel"); // ประเภทของไฟล์
//header('Content-Disposition: attachment; filename="myexcel.xls"'); //กำหนดชื่อไฟล์
//header("Content-Type: application/force-download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Transfer-Encoding: binary");
//header("Content-Length: ".filesize("myexcel.xls"));

@readfile($filename);
set_time_limit (60);
include '../connect.inc.php';
include '../../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('../document/31import_mem_type_id_vajira.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
$sheetData = $objPHPExcel->setActiveSheetIndex(0);
$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
//echo $yeartitle."<br>";
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

$data = array();
$i=0;
//echo '<pre>'; print_r($sheetData);exit;
$mem_type_sql = "SELECT mem_type_id, mem_type_name FROM `vajiracoop_sys`.`coop_mem_type`";
$result = $mysqli->query($mem_type_sql);
$mem_type_arr = array();
while($row_mem_type = $result->fetch_assoc()){
    $mem_type_arr[$row_mem_type['mem_type_name']] = $row_mem_type['mem_type_id'];
}
foreach($sheetData as $key => $value){
    if($key >= 4){
//		if($key >= 4 && $key <= 10){
        if($value['A'] == ''){
            break;
        }

        $mem_type_id = trim($value['N']);

        $data[$i]['member_id'] = sprintf("%06d", trim($value['B']));
        $work_date = trim($value['J']);
        if (!empty($work_date)) {
            $datearray = explode("-",$work_date);
            if(empty($datearray[1])){
                echo $data[$i]['member_id'].' '.$work_date.'<br>';
            }
            $meyear = $datearray[0] - 543;
            $work_date = $meyear . "-" . $datearray[1] . "-" . $datearray[2];
        }else{
            $work_date = '';
        }
        $retry_date = trim($value['M']);
        if (!empty($retry_date)) {
            $datearray = explode("-",$retry_date);
            if(empty($datearray[1])){
                echo  $data[$i]['member_id'].' '.$retry_date.'<br>';
            }
            $meyear = $datearray[0] - 543;
            $retry_date = $meyear . "-" . $datearray[1] . "-" . $datearray[2];
        }else{
            $retry_date = '';
        }
        $save_mem_type_id = $mem_type_id;
        $mem_type_id = str_replace(' วพบ',"",$mem_type_id);
        $mem_type_id = str_replace(' วพบ',"",$mem_type_id);
        $mem_type_id = str_replace(' วพก',"",$mem_type_id);
        $mem_type_id = str_replace(' สนพ',"",$mem_type_id);
        $mem_type_id = str_replace(' รพจ',"",$mem_type_id);
        $mem_type_id = str_replace(' รพก',"",$mem_type_id);
        $mem_type_id = str_replace(' รพต',"",$mem_type_id);
        $mem_type_id = str_replace(' รพว',"",$mem_type_id);
        $mem_type_id = str_replace(' วทส',"",$mem_type_id);
        $mem_type_id = str_replace(' รพร',"",$mem_type_id);
        $mem_type_id = str_replace(' รพน',"",$mem_type_id);
        $mem_type_id = str_replace(' รพล',"",$mem_type_id);
        $mem_type_id = str_replace(' รพท',"",$mem_type_id);
        $mem_type_id = str_replace(' รพส',"",$mem_type_id);
        $mem_type_id = str_replace(' รพค',"",$mem_type_id);
        $mem_type_id = str_replace(' รพข',"",$mem_type_id);
        $mem_type_id = str_replace(' สนธ',"",$mem_type_id);
        $mem_type_id = str_replace(' วชม',"",$mem_type_id);
        $mem_type_id = str_replace(' สนธ',"",$mem_type_id);
        $mem_type_id = str_replace(' วชม',"",$mem_type_id);
        $mem_type_id = str_replace(' รพส',"",$mem_type_id);
        $mem_type_id = str_replace(' รพบ',"",$mem_type_id);
        $mem_type_id = str_replace('.',"",$mem_type_id);

        if(!empty($mem_type_id) && $mem_type_id != '-'){
            $data[$i]['mem_type_id'] = $mem_type_arr[$mem_type_id];
        }else{
            $data[$i]['mem_type_id'] = '';
        }
        $data[$i]['work_date'] = $work_date;
        $data[$i]['retry_date'] = $retry_date;

        $i++;
    }
}
//echo '<pre>'; print_r($data);exit;
foreach ($data as $key => $value){
    $i++;
    if(!empty($value['mem_type_id']) || !empty($value['work_date']) || !empty($value['retry_date'])){
        $sql = "UPDATE `coop_mem_apply` SET";
        if(!empty($value['mem_type_id'])){
            $sql .= "`mem_type_id` = '{$value['mem_type_id']}',";
        }
        if(!empty($value['work_date'])){
            $sql .= "`work_date` = '{$value['work_date']}',";
        }
        if(!empty($value['retry_date'])){
            $sql .= "`retry_date` = '{$value['retry_date']}',";
        }
        $sql = substr($sql, 0,-1);
        $sql .= " WHERE `member_id` = '{$value['member_id']}';";
//        $sql = "UPDATE `coop_mem_apply` SET `mem_type_id` = '{$value['mem_type_id']}', `work_date` = '{$value['work_date']}', `retry_date` = '{$value['retry_date']}' WHERE `member_id` = '{$value['member_id']}';";
        echo $sql.'<br>';
//        exit;
    }
}


