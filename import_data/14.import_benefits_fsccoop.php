<?php
//header("Content-Type: application/vnd.ms-excel"); // ประเภทของไฟล์
//header('Content-Disposition: attachment; filename="myexcel.xls"'); //กำหนดชื่อไฟล์
//header("Content-Type: application/force-download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Transfer-Encoding: binary");
//header("Content-Length: ".filesize("myexcel.xls"));

@readfile($filename);
set_time_limit (60);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('document/import_benefits_fsccoop.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
$sheet = 1; // 0:ทั้งหมด 1:การศึกษา
$sheetData = $objPHPExcel->setActiveSheetIndex($sheet);
$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
//echo $yeartitle."<br>";
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

$datas = array();
if($sheet == 0){
    $i=1;
}else{
    $i=338;
}
//echo '<pre>'; print_r($sheetData);exit;
$benefits_type_id = '';
$benefits_subtype_id = NULL;
foreach($sheetData as $key => $value){
    if($key >= 1){
//		if($key >= 1 && $key < 4){
        if($value['B'] == ''){
            break;
        }

        if(!empty($value['A'])){
            if($sheet == 0){
                $benefits_type_id = trim($value['A']);
            }else{
                $benefits_type_id = 5;
                $benefits_subtype_id  = trim($value['A']);
                if(empty($benefits_subtype_id)){
                    $benefits_subtype_id = NULL;
                }
            }
        }else {

            $datas[$i]['benefits_request_id'] = $i;
            $datas[$i]['benefits_no'] = sprintf("%07d", $i);
            $datas[$i]['benefits_no'] = $datas[$i]['benefits_no'] . '/2563';
            $datas[$i]['runno'] = $i;
            $datas[$i]['yy'] = '2563';
            $datas[$i]['member_id'] = trim($value['B']);
            $datas[$i]['member_id'] = sprintf("%06d", $datas[$i]['member_id']);
            $datas[$i]['benefits_type_id'] = $benefits_type_id;
            $datas[$i]['benefits_subtype_id'] = $benefits_subtype_id;
            $datas[$i]['benefits_approved_amount'] = trim($value['C']);
            $datas[$i]['benefits_check_condition'] = 1;
            $datas[$i]['benefits_status'] = 1;
            $datas[$i]['user_id'] = 1;
            $datas[$i]['user_name'] = 'Administrator';

            $datas[$i]['createdatetime'] = trim($value['D']);
            $datas[$i]['updatetime'] = trim($value['E']);
            if($sheet != 0){
                $datas[$i]['GPA'] = trim($value['F']);
                $datas[$i]['internal_phone'] = trim($value['G']);
                $datas[$i]['tel'] = trim($value['H']);
                $datas[$i]['account_bank'] = trim($value['I']);
                $datas[$i]['son_name'] = trim($value['J']);
                if(empty($datas[$i]['GPA'])){
                    $datas[$i]['GPA'] = 'NULL';
                }
                if(empty($datas[$i]['internal_phone'])) {
                    $datas[$i]['internal_phone'] = 'NULL';
                }
                if(empty($datas[$i]['tel'])) {
                    $datas[$i]['tel'] = NULL;
                }
                if(empty($datas[$i]['account_bank'])) {
                    $datas[$i]['account_bank'] = NULL;
                }
                if(empty($datas[$i]['son_name'])) {
                    $datas[$i]['son_name'] = NULL;
                }
            }



            if (empty($datas[$i]['createdatetime'])) {
                $datas[$i]['createdatetime'] = '2019-10-01 00:00:00';
                $datas[$i]['updatetime'] = '2019-10-01 00:00:00';
            }else{
                if($sheet == 0){
                    $year = substr($datas[$i]['createdatetime'],0,4);
                    $year = $year-543;
                    $date = substr($datas[$i]['createdatetime'],4,10);
                    $datas[$i]['createdatetime'] = $year.$date;

                    $year = substr($datas[$i]['updatetime'],0,4);
                    $year = $year-543;
                    $date = substr($datas[$i]['updatetime'],4,10);
                    $datas[$i]['updatetime'] = $year.$date;
                }
            }
            $i++;
        }
    }
}
//echo '<pre>'; print_r($datas);exit;

foreach ($datas as $i => $data) {
    if($sheet == 0) {
        $sql = "INSERT INTO `coop_benefits_request`(`benefits_request_id`, `benefits_no`, `runno`, `yy`, `member_id`, `benefits_type_id`, `benefits_detail_id`, `benefits_approved_amount`, `benefits_check_condition`, `benefits_status`, `choices`, `user_id`, `user_name`, `note`, `createdatetime`, `updatetime`) VALUES (" . $i . ", '" . $datas[$i]['benefits_no'] . "', 1, '2563', '" . $datas[$i]['member_id'] . "', '" . $datas[$i]['benefits_type_id'] . "', NULL, '" . $datas[$i]['benefits_approved_amount'] . "', '1', '1', '', '1', 'Administrator', NULL, '" . $datas[$i]['createdatetime'] . "', '" . $datas[$i]['createdatetime'] . "');";
        echo $sql . '<br>';
        $sql = "INSERT INTO `coop_benefits_transfer`(`benefits_request_id`, `account_id`, `file_name`, `date_transfer`, `createdatetime`, `admin_id`, `transfer_status`, `cancel_date`, `benefits_transfer_id`, `bank_type`, `bank_id`, `bank_branch_id`, `bank_account_no`, `receipt_id`) VALUES (" . $i . ", NULL, NULL, '" . $datas[$i]['updatetime'] . "', '" . $datas[$i]['updatetime'] . "', 1, '0', NULL, " . $i . ", NULL, NULL, NULL, NULL, NULL);";
        echo $sql . '<br>';
    }else{
        $sql = "INSERT INTO `coop_benefits_request`(`benefits_request_id`, `benefits_no`, `runno`, `yy`, `member_id`, `benefits_type_id`, `benefits_subtype_id`, `benefits_detail_id`, `benefits_approved_amount`, `benefits_check_condition`, `benefits_status`, `choices`, `user_id`, `user_name`, `note`, `createdatetime`, `updatetime`, `GPA`, `internal_phone`, `tel`, `account_bank`, `son_name`) VALUES (" . $i . ", '" . $datas[$i]['benefits_no'] . "', 1, '2563', '" . $datas[$i]['member_id'] . "', '" . $datas[$i]['benefits_type_id']. "', '" . $datas[$i]['benefits_subtype_id'] . "', NULL, '" . $datas[$i]['benefits_approved_amount'] . "', '1', '1', '', '1', 'Administrator', NULL, '" . $datas[$i]['createdatetime'] . "', '" . $datas[$i]['createdatetime'] . "', " . $datas[$i]['GPA'] . ", '" . $datas[$i]['internal_phone'] . "', '" . $datas[$i]['tel'] . "', '" . $datas[$i]['account_bank'] . "', '" . $datas[$i]['son_name'] . "');";
        echo $sql . '<br>';

        if(!empty($datas[$i]['account_bank'])){
            $account_bank = $datas[$i]['account_bank'];
            $bank_type = '2';
        }
        $sql = "INSERT INTO `coop_benefits_transfer`(`benefits_request_id`, `account_id`, `file_name`, `date_transfer`, `createdatetime`, `admin_id`, `transfer_status`, `cancel_date`, `benefits_transfer_id`, `bank_type`, `bank_id`, `bank_branch_id`, `bank_account_no`, `receipt_id`) VALUES (" . $i . ", NULL, NULL, '" . $datas[$i]['updatetime'] . "', '" . $datas[$i]['updatetime'] . "', 1, '0', NULL, " . $i . ", '2', NULL, NULL, '".$data['account_bank']."', NULL);";
        echo $sql . '<br>';
    }
}
// update coop_benefits_request
