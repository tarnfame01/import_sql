<?php
//header("Content-Type: application/vnd.ms-excel"); // ประเภทของไฟล์
//header('Content-Disposition: attachment; filename="myexcel.xls"'); //กำหนดชื่อไฟล์
//header("Content-Type: application/force-download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Transfer-Encoding: binary");
//header("Content-Length: ".filesize("myexcel.xls"));

@readfile($filename);
set_time_limit (60);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('document/27venus_member.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');

$sheetData = $objPHPExcel->setActiveSheetIndex(1);
$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

$data = array();
$i = 0;
$date_now = date("Y/m/d H:i:s", strtotime('+6 hour'));
foreach ($sheetData as $key => $value) {
    if ($key >= 2) {
//		if($key >= 18 && $key <= 20){
        if ($value['A'] == '') {
            break;
        }

//        $data[$i]['member_id'] = sprintf("%06d", trim($value['A']));
        $data[$i]['member_id'] = trim($value['A']);
        $data[$i]['share_date'] = trim($value['B']);
        $data[$i]['share_month'] = trim($value['C']);
        if(!empty($value['C'])){
            $data[$i]['share_early'] = trim($value['C'])/10;
        }else{
            $data[$i]['share_early'] = 0;
        }
        $data[$i]['share_early_value'] = trim($value['C']);
        if(!empty($value['D'])){
            $data[$i]['share_collect'] = trim($value['D'])/10;
        }else{
            $data[$i]['share_collect'] = 0;
        }
        $data[$i]['share_collect_value'] = trim($value['D']);
        $i++;
    }
}
//echo '<pre>'; print_r($data);exit;
$group_arr = array();
foreach ($data as $key => $value){
    $sql_insert= "INSERT coop_mem_share SET
        member_id = '".$value['member_id']."',
        share_date = '".$value['share_date']."',
        share_type = 'SPM',
        share_early = '".$value['share_early']."',
        share_early_value = '".$value['share_early_value']."',
        share_collect = '".$value['share_collect']."',
        share_collect_value = '".$value['share_collect_value']."'
        ;";
    echo $sql_insert.'<br>';


    $sql_update = "UPDATE `coop_mem_apply` SET `share_month` = '".$value['share_month']."' WHERE `member_id` = '".$value['member_id']."';";
    echo $sql_update.'<br>';
}

