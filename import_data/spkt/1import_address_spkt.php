<?php
//header("Content-Type: application/vnd.ms-excel"); // ประเภทของไฟล์
//header('Content-Disposition: attachment; filename="myexcel.xls"'); //กำหนดชื่อไฟล์
//header("Content-Type: application/force-download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Transfer-Encoding: binary");
//header("Content-Length: ".filesize("myexcel.xls"));

@readfile($filename);
set_time_limit (60);
include '../connect.inc.php';
include '../../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('../document/32import_address_spkt.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');

$sheetData = $objPHPExcel->setActiveSheetIndex(0);
$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

$data = array();
$i = 0;
//echo '<pre>'; print_r($sheetData);exit;

foreach ($sheetData as $key => $value) {
    if ($key >= 2) {
//		if($key >= 18 && $key <= 20){
        if ($value['A'] == '') {
            break;
        }

        $data[$i]['member_id'] = sprintf("%06d", trim($value['A']));
        $data[$i]['d_address_no'] = trim($value['C']);
        $data[$i]['d_address_moo'] = trim($value['D']);
        $data[$i]['d_address_village'] = trim($value['E']);
        $data[$i]['d_address_road'] = trim($value['G']);
        $data[$i]['d_address_soi'] = trim($value['F']);
        $data[$i]['d_province_name'] = trim($value['J']);
        $data[$i]['d_amphur_name'] = trim($value['I']);
        $data[$i]['d_district_name'] = trim($value['H']);
//        $data[$i]['d_province_id'] = trim($value['J']);
//        $data[$i]['d_amphur_id'] = trim($value['I']);
//        $data[$i]['d_district_id'] = trim($value['H']);
        $data[$i]['d_zipcode'] = trim($value['K']);
        $i++;
    }
}
//echo '<pre>'; print_r($data);echo '</pre>';
//$str = " ตัวอย่าง   ข้อความ ที่ใช้      ในการทดสอบ    ";
// ใช้ฟังก์ชัน trim ตัดช่องว่าง ด้านหน้าและด้านหลัง
// ตัดช่องว่างภายในข้อความออกทั้งหมด
// ผลลัพธ์ที่ได้ "ตัวอย่างข้อความที่ใช้ในการทดสอบ"
//$newStr = ereg_replace('[[:space:]]+', '', trim($str));
//echo $newStr;exit;
$leave = substr($data[85]['d_amphur_name'], -2); // ใช้แทน ' '
foreach ($data as $key => $item) {
    $item['d_province_name'] = str_replace('สมุทปราการ',"สมุทรปราการ",$item['d_province_name']);
    $item['d_province_name'] = str_replace('สมุทรปารการ',"สมุทรปราการ",$item['d_province_name']);
    $item['d_province_name'] = str_replace('สุมุทรปราการ',"สมุทรปราการ",$item['d_province_name']);
//    $item['d_province_name'] = str_replace('10240',"",$item['d_province_name']);

    $province_id = "";
    if(!empty($item['d_province_name'])) {
        $sql_district = "SELECT province_id FROM `spktsys_com`.`coop_province` WHERE `province_name` LIKE '%{$item['d_province_name']}%'";
        $rs_district = $mysqli->query($sql_district);

        while ($row_district = $rs_district->fetch_assoc()) {
            $province_id = $row_district['province_id'];
        }
    }
    if(!empty($province_id)){
        $data[$key]['province_id'] = $province_id;
    }
    if(empty($province_id) && !empty($item['d_province_name'])){
        echo 'district<br>';
        echo $sql_district.'<br>';
        echo $item['d_province_name'].'<br>';
        echo $province_id.'<br>';
        exit;
    }


    $amphur_id = "";

    $item['d_amphur_name'] = str_replace(' ','',$item['d_amphur_name']);
    $item['d_amphur_name'] = str_replace($leave,'',$item['d_amphur_name']);
    $item['d_amphur_name'] = str_replace('บ่างบ่อ','บางบ่อ',$item['d_amphur_name']);
    $item['d_amphur_name'] = str_replace('ขุญหาญ','ขุนหาญ',$item['d_amphur_name']);

    if(!empty($item['d_amphur_name'])) {
        $sql_amphur = "SELECT amphur_id FROM `spktsys_com`.`coop_amphur` WHERE `amphur_name` LIKE '%{$item['d_amphur_name']}%'";
        $rs_amphur = $mysqli->query($sql_amphur);
        while ($row_amphur = $rs_amphur->fetch_assoc()) {
            $amphur_id = $row_amphur['amphur_id'];
        }
    }
    if(!empty($amphur_id)){
        $data[$key]['amphur_id'] = $amphur_id;
    }
    if(empty($amphur_id) && !empty($item['d_amphur_name'])){
        echo $key.'<br>';
        echo 'amphur<br>';
        echo $sql_amphur.'<br>';
        echo $item['d_amphur_name'].'<br>';
        echo $amphur_id.'<br>';
        exit;
    }

    $district_id = "";

    if(!empty($item['d_district_name'])) {
        $sql_district = "SELECT district_id, district_name FROM `spktsys_com`.`coop_district` WHERE `district_name` = '{$item['d_district_name']}'";
        $rs_district = $mysqli->query($sql_district);
        while ($row_district = $rs_district->fetch_assoc()) {
            $district_id = $row_district['district_id'];
            $district_name = $row_district['district_name'];
        }
        if (empty($district_id)) {
            $sql_district = "SELECT district_id, district_name FROM `spktsys_com`.`coop_district` WHERE `district_name` LIKE '%{$item['d_district_name']}%'";
            $rs_district = $mysqli->query($sql_district);
            while ($row_district = $rs_district->fetch_assoc()) {
                $district_id = $row_district['district_id'];
                $district_name = $row_district['district_name'];
            }
        }
    }

    if(!empty($district_id)){
        $data[$key]['district_id'] = $district_id;
        $data[$key]['district_name'] = $district_name;
    }
    if(empty($rs_district) && !empty($item['d_district_name'])){
        echo $sql_district.'<br>';
        echo $item['d_district_name'].'<br>';
        echo $district_id.'<br>';
        exit;
    }

    if(empty($province_id)){$province_id = 'null';}
    if(empty($amphur_id)){$amphur_id = 'null';}
    if(empty($district_id)){$district_id = 'null';}

    $sql = "DELETE FROM `coop_debt_address` WHERE `member_id` = '{$item['member_id']}';";
    echo $sql.'<br>';
    $sql = "INSERT INTO `coop_debt_address`(`addebt_id`, `member_id`, `d_address_no`, `d_address_moo`, `d_address_village`, `d_address_road`, `d_address_soi`, `d_province_id`, `d_amphur_id`, `d_district_id`, `d_zipcode`, `d_tel`, `createdate`) VALUES (NULL, '{$item['member_id']}', '{$item['d_address_no']}', '{$item['d_address_moo']}', '{$item['d_address_village']}', '{$item['d_address_road']}', '{$item['d_address_soi']}', {$province_id}, {$amphur_id}, {$district_id}, '{$item['d_zipcode']}', NULL, '2022-02-24 10:24:19');";
    echo $sql.'<br>';
}
//echo '<pre>'; print_r($data);exit;