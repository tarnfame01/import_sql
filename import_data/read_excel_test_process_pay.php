<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<?php 
set_time_limit (60);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
//$objPHPExcel = PHPExcel_IOFactory::load('document/test_process.xlsx');
$objPHPExcel = PHPExcel_IOFactory::load('document/test_finance_month_2018_08.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
	$sheetData = $objPHPExcel->setActiveSheetIndex(0);
	$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
	//echo $yeartitle."<br>";
	$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
	//echo"<pre>";print_r($sheetData);exit;
	$data_date = '2018-08-31';
	$profile_id = '32';
	$data = array();
	$i=0;
	foreach($sheetData as $key => $value){
		if($key >= 5){
			$data[$i]['member_id'] = trim($value['C']);
			$data[$i]['share'] = trim($value['E']);
			
			$j=0;
			$data[$i]['loan'][$j]['contract_number'] = trim($value['F']);
			$data[$i]['loan'][$j]['principal'] = trim($value['G']);
			$data[$i]['loan'][$j]['interest'] = trim($value['H']);
			$contract_code = explode('61',$data[$i]['loan'][$j]['contract_number']);
			$contract_code = explode('60',$contract_code[0]);
			$contract_code = explode('59',$contract_code[0]);
			$contract_code = explode('58',$contract_code[0]);
			$contract_code = explode('57',$contract_code[0]);
			$contract_code = explode('56',$contract_code[0]);
			$contract_code = explode('55',$contract_code[0]);
			if($contract_code[0] == 'ฉอ' || $contract_code[0] == 'ฉด'){
				$data[$i]['loan'][$j]['loan_type'] = 'atm';
			}else{
				$data[$i]['loan'][$j]['loan_type'] = 'emergent';
			}
			$j++;
			$data[$i]['loan'][$j]['contract_number'] = trim($value['I']);
			$data[$i]['loan'][$j]['principal'] = trim($value['J']);
			$data[$i]['loan'][$j]['interest'] = trim($value['K']);
			$data[$i]['loan'][$j]['loan_type'] = 'normal';
			
			$j++;
			$data[$i]['loan'][$j]['contract_number'] = trim($value['L']);
			$data[$i]['loan'][$j]['principal'] = trim($value['M']);
			$data[$i]['loan'][$j]['interest'] = trim($value['N']);
			$data[$i]['loan'][$j]['loan_type'] = 'special';
			
			$data[$i]['deposit_account_id'] = trim($value['O']);
			$data[$i]['deposit_amount'] = trim($value['P']);
			
			$i++;
		}
	}
	//echo"<pre>";print_r($data);exit;
	foreach($data as $key => $value){
		for($i=1;$i<=2;$i++){
			foreach($value['loan'] as $key_loan => $value_loan){
				if($value_loan['contract_number']!=''){
					if($i==1){
						$pay_type = 'interest';
						$pay_amount = $value_loan['interest'];
					}else{
						$pay_type = 'principal';
						$pay_amount = $value_loan['principal'];
					}
					if($value_loan['loan_type'] == 'atm'){
						$sql = "SELECT loan_atm_id FROM coop_loan_atm WHERE contract_number = '".$value_loan['contract_number']."'";
						$rs = $mysqli->query($sql);
						$row = $rs->fetch_assoc();
						$loan_atm_id = @$row['loan_atm_id'];
						$loan_id = '';
						$deduct_code = 'ATM';
						
						$sql_update = "UPDATE coop_loan_atm_detail SET 
							principal_per_month = '".$value_loan['principal']."' 
						WHERE 
							loan_atm_id = '".$loan_atm_id."'
						";
						if(@$_GET['runscript']=='runscript'){
							$mysqli->query($sql_update);
						}
						echo $sql_update."<br>";
					}else{
						$sql = "SELECT id FROM coop_loan WHERE contract_number = '".$value_loan['contract_number']."'";
						//echo $sql;
						$rs = $mysqli->query($sql);
						$row = $rs->fetch_assoc();
						$loan_atm_id = '';
						$loan_id = @$row['id'];
						$deduct_code = 'LOAN';
						
						$total_pay = ($value_loan['principal']+$value_loan['interest']);
						
						if($i==1){
							$sql_insert = "INSERT INTO coop_loan_period SET 
								loan_id = '".$loan_id."',
								period_count = '1',
								date_period = '".$data_date."',
								date_count = '31',
								interest = '".$value_loan['interest']."',
								principal_payment = '".$value_loan['principal']."',
								total_paid_per_month = '".$total_pay."'
							";
							if(@$_GET['runscript']=='runscript'){
								$mysqli->query($sql_insert);
							}
							echo $sql_insert."<br>";
						}
						
						if(($value_loan['principal']%100)==0){
							$pay_loan_type = '1';
						}else{
							$pay_loan_type = '2';
						}
						$sql_update = "UPDATE coop_loan SET 
							money_period_1 = '".$total_pay."',
							date_start_period = '".$data_date."',
							pay_type = '".$pay_loan_type."'
						WHERE 
							id = '".$loan_id."'
						";
						if(@$_GET['runscript']=='runscript'){
							$mysqli->query($sql_update);
						}
						echo $sql_update."<br>";
					}
					if($value_loan['loan_type'] == 'atm'){
						if($i==1){
							$deduct_id = '20';
						}else{
							$deduct_id = '21';
						}
					}else if($value_loan['loan_type'] == 'emergent'){
						if($i==1){
							$deduct_id = '1';
						}else{
							$deduct_id = '5';
						}
					}else if($value_loan['loan_type'] == 'normal' || $value_loan['loan_type'] == 'special'){
						if($i==1){
							$deduct_id = '2';
						}else{
							$deduct_id = '7';
						}
					}
					if($loan_atm_id!='' || $loan_id !=''){
						$sql_insert = "INSERT INTO coop_finance_month_detail SET 
							profile_id = '".$profile_id."',
							member_id = '".$value['member_id']."',
							deduct_code = '".$deduct_code."',
							loan_id = '".$loan_id."',
							loan_atm_id = '".$loan_atm_id."',
							pay_amount = '".$pay_amount."',
							real_pay_amount = '".$pay_amount."',
							pay_type = '".$pay_type."',
							deduct_id = '".$deduct_id."',
							run_status = '0'
						";
						if(@$_GET['runscript']=='runscript'){
							$mysqli->query($sql_insert);
						}
						echo $sql_insert."<br>";
					}else{
						echo $value_loan['contract_number']."<br>";
					}
				}
			}
		}
		if($value['share'] != ''){
			$sql_insert = "INSERT INTO coop_finance_month_detail SET 
				profile_id = '".$profile_id."',
				member_id = '".$value['member_id']."',
				deduct_code = 'SHARE',
				pay_amount = '".$value['share']."',
				real_pay_amount = '".$value['share']."',
				pay_type = 'principal',
				deduct_id = '14',
				run_status = '0'
			";
			if(@$_GET['runscript']=='runscript'){
				$mysqli->query($sql_insert);
			}
			echo $sql_insert."<br>";
			
			$sql_insert = "UPDATE INTO coop_mem_apply SET 
				share_month = '".$value['share']."',
			WHERE 
				member_id = '".$value['member_id']."'
			";
			if(@$_GET['runscript']=='runscript'){
				$mysqli->query($sql_insert);
			}
			echo $sql_insert."<br>";
		}
		if($value['deposit_account_id']!=''){
			$sql_insert = "INSERT INTO coop_finance_month_detail SET 
				profile_id = '".$profile_id."',
				member_id = '".$value['member_id']."',
				deduct_code = 'DEPOSIT',
				pay_amount = '".$value['deposit_amount']."',
				real_pay_amount = '".$value['deposit_amount']."',
				deposit_account_id = '".$value['deposit_account_id']."',
				pay_type = 'principal',
				deduct_id = '15',
				run_status = '0'
			";
			if(@$_GET['runscript']=='runscript'){
				$mysqli->query($sql_insert);
			}
			echo $sql_insert."<br>";
		}
		
		echo "_______________________________<br>";
	}
?>