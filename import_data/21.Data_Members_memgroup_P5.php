<?php
//header("Content-Type: application/vnd.ms-excel"); // ประเภทของไฟล์
//header('Content-Disposition: attachment; filename="myexcel.xls"'); //กำหนดชื่อไฟล์
//header("Content-Type: application/force-download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Transfer-Encoding: binary");
//header("Content-Length: ".filesize("myexcel.xls"));

@readfile($filename);
set_time_limit (6000);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('document/15mem_group_p5.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
$sheetData = $objPHPExcel->setActiveSheetIndex(0);
$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
//echo $yeartitle."<br>";
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

$data = array();
$i=0;
//	echo '<pre>'; print_r($sheetData);exit;
foreach($sheetData as $key => $value){
    if($key >= 2){
//    if($key >= 3 && $key <=10){
//        if($value['A'] == ''){
//            break;
//        }

        $department = trim($value['A']);
        $department_name = trim($value['B']);
        $faction = trim($value['C']);
        $faction_name = trim($value['D']);
        $level = trim($value['E']);
        $level_name = trim($value['F']);
        $data[$i]['department']['id'] = $department;
        $data[$i]['department']['name'] = $department_name;
        $data[$i]['faction']['id'] = $faction;
        $data[$i]['faction']['name'] = $faction_name;
        $data[$i]['level']['id'] = $level;
        $data[$i]['level']['name'] = $level_name;
        $i++;
    }
}
//echo '<pre>'; print_r($data);echo '</pre>';
echo '<hr>';
$new_data = array();
$department = '';
$department_name = '';
$faction = '';
$faction_name = '';
foreach ($data as $key => $value){
    if(!empty($value['department']['name'])){
        $department = $value['department']['id'];
        $department_name = $value['department']['name'];
        $department = array('mem_group' => $value['department']['id'], 'name' => $department_name);
        $new_data[$department_name] = $department;
    }

    if(!empty($value['faction']['name'])){
        $faction_name = $value['faction']['name'];
        $faction = array('mem_group' => $value['faction']['id'], 'name' => $faction_name);
        $new_data[$department_name]['faction'][$faction_name] = $faction;
    }

    if(!empty($value['level']['name'])){
        $level_name = $value['level']['name'];
        $level = array('mem_group' => $value['level']['id'], 'name' => $level_name);
        $new_data[$department_name]['faction'][$faction_name]['level'][] = $level;
    }
}
//echo '<pre>'; print_r($data);exit;
//echo '<pre>'; print_r($new_data);echo '</pre>';
echo '<table>';
$i = 1;
echo '<tr>';
echo '<th>'; echo 'ID';echo '</th>';
echo '<th>'; echo 'mem_group_parent_id';echo '</th>';
echo '<th>'; echo 'mem_group_id';echo '</th>';
echo '<th>'; echo 'name';echo '</th>';
echo '<th>'; echo 'level';echo '</th>';
echo '</tr>';
foreach ($new_data as $key => $value){
    echo '<tr>';
    echo '<td>'; echo $i  ;echo '</td>';
    echo '<th>'; echo "" ;echo '</th>';
    echo '<th>'; echo $value['mem_group'] ;echo '</th>';
    echo '<td>'; echo $value['name'] ;echo '</td>';
    echo '<th>'; echo 1 ;echo '</th>';
    echo '</tr>';
    $sql = "INSERT INTO `coop_mem_group`(`mem_group_id`, `mem_group_parent_id`, `mem_group_type`, `mem_group_name`, `mem_group_full_name`, `id`) VALUES ('".$value['mem_group']."', '', '1', '".$value['name']."', '".$value['name']."', ".$i.");";
    echo $sql.'<br>';

    $mem_group_parent_id = $i;
    $i++;
    if(!empty($value['faction'])){
        foreach ($value['faction'] as $faction => $faction_value){
            echo '<tr>';
            echo '<td>'; echo $i  ;echo '</td>';
            echo '<th>'; echo $mem_group_parent_id ;echo '</th>';
            echo '<th>'; echo $faction_value['mem_group'] ;echo '</th>';
            echo '<td>'; echo $faction_value['name'] ;echo '</td>';
            echo '<th>'; echo 2 ;echo '</th>';
            echo '</tr>';
            $sql = "INSERT INTO `coop_mem_group`(`mem_group_id`, `mem_group_parent_id`, `mem_group_type`, `mem_group_name`, `mem_group_full_name`, `id`) VALUES ('".$faction_value['mem_group']."', ".$mem_group_parent_id.", '2', '".$faction_value['name']."', '".$faction_value['name']."', ".$i.");";
            echo $sql.'<br>';

            $mem_group_parent_id2 = $i;
            $i++;
            if(!empty($faction_value['level'])) {
                foreach ($faction_value['level'] as $level => $level_value) {
                    echo '<tr>';
                    echo '<td>'; echo $i ;echo '</td>';
                    echo '<th>'; echo $mem_group_parent_id2 ;echo '</th>';
                    echo '<th>'; echo $level_value['mem_group'] ;echo '</th>';
                    echo '<td>'; echo $level_value['name'] ;echo '</td>';
                    echo '<th>'; echo 3 ;echo '</th>';
                    echo '</tr>';
                    $sql = "INSERT INTO `coop_mem_group`(`mem_group_id`, `mem_group_parent_id`, `mem_group_type`, `mem_group_name`, `mem_group_full_name`, `id`) VALUES ('".$level_value['mem_group']."', ".$mem_group_parent_id2.", '3', '".$level_value['name']."', '".$level_value['name']."', ".$i.");";
                    echo $sql.'<br>';

                    $i++;
                }
            }else{
                echo '<tr>';
                echo '<td>'; echo $i ;echo '</td>';
                echo '<th>'; echo $mem_group_parent_id2 ;echo '</th>';
                echo '<th>'; echo $faction_value['mem_group'] ;echo '</th>';
                echo '<td>'; echo $faction_value['name'] ;echo '</td>';
                echo '<th>'; echo 3 ;echo '</th>';
                echo '</tr>';
                $sql = "INSERT INTO `coop_mem_group`(`mem_group_id`, `mem_group_parent_id`, `mem_group_type`, `mem_group_name`, `mem_group_full_name`, `id`) VALUES ('".$faction_value['mem_group']."', ".$mem_group_parent_id2.", '3', '".$faction_value['name']."', '".$faction_value['name']."', ".$i.");";
                echo $sql.'<br>';

                $i++;
            }
        }
    }else{
        echo '<tr>';
        echo '<td>'; echo $i  ;echo '</td>';
        echo '<th>'; echo $mem_group_parent_id ;echo '</th>';
        echo '<th>'; echo $value['mem_group'] ;echo '</th>';
        echo '<td>'; echo $value['name'] ;echo '</td>';
        echo '<th>'; echo 2 ;echo '</th>';
        echo '</tr>';
        $sql = "INSERT INTO `coop_mem_group`(`mem_group_id`, `mem_group_parent_id`, `mem_group_type`, `mem_group_name`, `mem_group_full_name`, `id`) VALUES ('".$value['mem_group']."', ".$mem_group_parent_id.", '2', '".$value['name']."', '".$value['name']."', ".$i.");";
        echo $sql.'<br>';

        $mem_group_parent_id = $i;
        $i++;
        echo '<tr>';
        echo '<td>'; echo $i  ;echo '</td>';
        echo '<th>'; echo $mem_group_parent_id ;echo '</th>';
        echo '<th>'; echo $value['mem_group'] ;echo '</th>';
        echo '<td>'; echo $value['name'] ;echo '</td>';
        echo '<th>'; echo 3 ;echo '</th>';
        echo '</tr>';
        $sql = "INSERT INTO `coop_mem_group`(`mem_group_id`, `mem_group_parent_id`, `mem_group_type`, `mem_group_name`, `mem_group_full_name`, `id`) VALUES ('".$value['mem_group']."', ".$mem_group_parent_id.", '3', '".$value['name']."', '".$value['name']."', ".$i.");";
        echo $sql.'<br>';

        $mem_group_parent_id = $i;
        $i++;
    }


}
?>



