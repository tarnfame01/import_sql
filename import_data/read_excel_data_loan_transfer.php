<?php 
set_time_limit (60);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('document/data_loan.xls');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
	$sheetData = $objPHPExcel->setActiveSheetIndex(0);
	$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
	//echo $yeartitle."<br>";
	$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
	
	$data = array();
	$i=0;
	foreach($sheetData as $key => $value){		
		if($key >= 6){
		//if($key >= 3 && $key <= 20){
			//if($value['A'] == ''){
			//	break;
			//}

			$data[$i]['member_id'] = trim($value['B']);
			$data[$i]['full_name'] = trim($value['C']);//ชื่อ - สกุล
			
			$date = explode('-',trim($value['E']));
			//echo '<pre>'; print_r($b1); echo '</pre>';
			$date_d = $date[1];
			$date_m = $date[0];
			$date_y = '25'.$date[2];
			//$approve_date = ($date_y-543).'-'.$date_m.'-'.$date_d;			
			$approve_date = ($date_y-543).'-'.$date_m.'-'.$date_d;			
			//echo '<pre>'; print_r(trim($value['E'])); echo '</pre>';
			//echo '<pre>'; print_r($approve_date); echo '</pre>';
			
			$data[$i]['approve_date'] = $approve_date;//วันที่กู้
			$data[$i]['contract_number'] = trim($value['F']);//เลขสัญญา
			$data[$i]['loan_amount'] = trim($value['G']);//จำนวนเงินกู้
			$data[$i]['period_amount'] = trim($value['H']);//จำนวนงวดชำระ
			$data[$i]['money_per_period'] = trim($value['I']);//งวดละ	
			
			if(trim($value['J']) != ''){
				$period_y = substr(trim($value['J']), -2);	
				$period_m = $month_short_arr[str_replace($period_y,"",trim($value['J']))];		
				//$date_period_y = ($period_y <2562)?'24'.$period_y :'25'.$period_y ;
				$date_period_y = '25'.$period_y ;
				
				//echo '<pre>'; print_r(trim($value['J'])); echo '</pre>';
				//echo '<pre>'; print_r($period_m); echo '</pre>';
				//echo '<pre>'; print_r($date_period_y); echo '</pre>';
				$date_period = ($date_period_y-543).'-'.$period_m.'-01';
				$date_period2 = date("Y-m-t", strtotime($date_period));
			}else{
				$date_period2 = '';
			}
			//echo '<pre>'; print_r($date_period); echo '</pre>';
			//echo '<pre>'; print_r($date_period2); echo '</pre>';
			$data[$i]['date_start_period'] = $date_period2;//หักงวดแรกเดือน
			$data[$i]['period_now'] = trim($value['K']);//ชำระล่าสุดงวดที่ 
			$data[$i]['loan_amount_balance'] = trim($value['L']);//หนี้คงเหลือ
			$data[$i]['createdatetime'] = '2019-03-31';
			$data[$i]['updatetimestamp'] = '2019-03-31';

			$sql1 = "SELECT id FROM coop_loan WHERE contract_number = '".$data[$i]['contract_number'] ."'";
			$rs1 = $mysqli->query($sql1);
			$row1 = $rs1->fetch_assoc();
			$data[$i]['loan_id'] = $row1['id'];
			
			$i++;
		}
	}
	//exit;
	/*echo '<pre>'; print_r($data); echo '</pre>';	
	echo '<table border= "1" width="90%">';
		echo '<tr>';
			echo '<td>member_id</td>';
			echo '<td>ชื่อ - สกุล</td>';
			echo '<td>วันที่กู้</td>';
			echo '<td>เลขสัญญา</td>';
			echo '<td>จำนวนเงินกู้</td>';
			echo '<td>จำนวนงวดชำระ</td>';
			echo '<td>งวดละ</td>';
			echo '<td>หักงวดแรกเดือน</td>';
			echo '<td>ชำระล่าสุดงวดที่</td>';
			echo '<td>หนี้คงเหลือ</td>';
		echo '</tr>';
		*/
	
	foreach($data as $key => $value){			
		if($value['loan_id'] != ''){
			//coop_loan_transfer
			$sql_insert= "INSERT coop_loan_transfer SET 
			loan_id = '".$value['loan_id']."',
			date_transfer = '".$value['approve_date']."',
			admin_id = '1',
			transfer_status = '0'			
			;";
			echo $sql_insert.'<br>';
			
			//coop_loan_period
			$sql_insert= "INSERT coop_loan_period SET 
			loan_id = '".$value['loan_id']."',
			period_count = '1',	
			outstanding_balance = '".$value['loan_amount']."',	
			date_period = '".$value['date_start_period']."',	
			date_count = '31',	
			interest = '0',	
			principal_payment = '".$value['money_per_period']."',	
			total_paid_per_month = '".$value['money_per_period']."'
			;";
			echo $sql_insert.'<br>';
			
			//coop_loan_transaction
				$sql_insert= "INSERT coop_loan_transaction SET 
			loan_id = '".$value['loan_id']."',
			loan_amount_balance = '".$value['loan_amount_balance']."',
			transaction_datetime = '".$value['approve_date']."'		
			;";
			echo $sql_insert.'<br>';

		}
	}
	//echo '</table>';
	//echo "<pre>"; print_r($data);
	exit;
?>