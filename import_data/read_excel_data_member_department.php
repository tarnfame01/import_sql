<?php 
set_time_limit (60);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('document/data_member_glocoop.xls');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
	$sheetData = $objPHPExcel->setActiveSheetIndex(0);
	$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
	//echo $yeartitle."<br>";
	$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
	
	$data = array();
	$i=0;
	foreach($sheetData as $key => $value){
		
		if($key >= 3){
			if($value['A'] == ''){
				break;
			}
			//echo '<pre>'; print_r($value); echo '</pre>';
			//$data[$i]['main_department_id'] = substr(trim($value['A']),0,2);
			$data[$i]['runno'] = trim($value['A']);
			$data[$i]['member_id'] = trim($value['B']);
			
			$data[$i]['prename'] = trim($value['C']);
			$sql1 = "SELECT * FROM coop_prename WHERE prename_short = '".trim($value['C'])."'";
			$rs1 = $mysqli->query($sql1);
			$row1 = $rs1->fetch_assoc();
			
			$data[$i]['prename_id'] = $row1['prename_id'];
			$data[$i]['sex'] = $row1['sex'];
		
			$data[$i]['firstname_th'] = trim($value['D']);
			$data[$i]['lastname_th'] = trim($value['E']);
			$data[$i]['department_full'] = trim($value['F']);
			//$data[$i]['department_code'] = trim($value['G']);
			//echo '<pre>'; print_r($value); echo '</pre>';
			$data[$i]['department_code'] = trim($value['G']);
			if(trim($value['G']) == 'J20'){
				$data[$i]['department_code'] = 'P10';
			}
			if(trim($value['G']) == 'J21'){
				$data[$i]['department_code'] = 'P11';
			}
			if(trim($value['G']) == 'P12'){
				$data[$i]['department_code'] = 'P11';
			}
			if(trim($value['G']) == 'J23'){
				$data[$i]['department_code'] = 'P13';
			}
			if(trim($value['G']) == 'J30'){
				$data[$i]['department_code'] = 'P20';
			}
			if(trim($value['G']) == 'J31'){
				$data[$i]['department_code'] = 'P21';
			}
			if(trim($value['G']) == 'J32'){
				$data[$i]['department_code'] = 'P22';
			}
			if(trim($value['G']) == 'K30'){
				$data[$i]['department_code'] = 'J20';
			}
			if(trim($value['G']) == 'K31'){
				$data[$i]['department_code'] = 'J21';
			}
			if(trim($value['G']) == 'K34'){
				$data[$i]['department_code'] = 'J22';
			}
			if(trim($value['G']) == 'K32'){
				$data[$i]['department_code'] = 'K30';
			}
			if(trim($value['G']) == 'K33'){
				$data[$i]['department_code'] = 'K31';
			}

			$data[$i]['share_month'] = trim($value['I']);
			$data[$i]['salary'] = trim($value['K']);
			$b1 = explode('-',trim($value['L']));
			//echo '<pre>'; print_r($b1); echo '</pre>';
			$b_d = $b1[1];
			$b_m = $b1[0];
			$b_y = ($b1[2]<2562)?'24'.$b1[2]:'25'.$b1[2];
			//$birthday = $b_y.'-'.$b_m.'-'.$b_d;
			$birthday = ($b_y-543).'-'.$b_m.'-'.$b_d;
			$data[$i]['birthday'] = $birthday;
			$data[$i]['nationality'] = trim($value['M']);
			$m1 = explode('/',trim($value['N']));
			//echo '<pre>'; print_r($b1); echo '</pre>';
			$m_d = $m1[0];
			$m_m = $m1[1];
			$m_y = $m1[2];
			$member_date = ($m_y-543).'-'.$m_m.'-'.$m_d;
			$member_date = date('Y-m-d',strtotime($member_date));
			$data[$i]['member_date'] = $member_date;
			$data[$i]['address'] = trim($value['O']);
			$i++;
		}
	}
	
	/*$sql_mem = "SELECT member_id FROM coop_mem_apply WHERE apply_type_id = '1'";
	$rs_mem = $mysqli->query($sql_mem);
	$arr_member = array();
	while($row_mem = $rs_mem->fetch_assoc()){
		$arr_member[$row_mem['member_id']] = $row_mem['member_id'];
		//echo '<pre>'; print_r($row_mem); echo '</pre>';
	}
*/	
	//echo '<pre>'; print_r($arr_member); echo '</pre>';
	/*
	$sql_pre= "SELECT * FROM coop_prename";
	$rs_pre = $mysqli->query($sql_pre);
	$arr_prename = array();
	while($row_pre = $rs_pre->fetch_assoc()){
		$arr_prename[$row_pre['prename_id']] = $row_pre['prename_id'];
		//echo '<pre>'; print_r($row_pre); echo '</pre>';
	}
	echo '<pre>'; print_r($arr_prename); echo '</pre>';
	*/
	//$data[$key]['prename_id'] = $row1['prename_id'];
	//$data[$key]['sex'] = $row1['sex'];
	$arr_member = array(
'000514',
'000602',
'000713',
'000744',
'000745',
'000748',
'000749',
'000750',
'000847',
'000880',
'000927',
'000950',
'000984',
'000999',
'001055',
'001070',
'001101',
'001102',
'001103',
'001104',
'001115',
'001137',
'001146',
'001148',
'001149',
'001156',
'001188',
'001191',
'001195',
'001206',
'001233',
'001234',
'001256',
'001273',
'001278',
'001282',
'001304',
'001359',
'001372',
'001375',
'001379',
'001388',
'001389',
'001436',
'001440',
'001461',
'001462',
'001464',
'001479',
'001497',
'001563',
'001564',
'001604',
'001613',
'001636',
'001638'
	);
	
	$run = 15;
	foreach($data as $key => $value){

		if(in_array($value['member_id'],$arr_member)){
			$sql2 = "SELECT mem_group_id,mem_group_parent_id,mem_group_type,mem_group_name,id FROM coop_mem_group WHERE mem_group_id = '".$value['department_code']."'";
			$rs2 = $mysqli->query($sql2);
			$row2 = $rs2->fetch_assoc();
			//echo $sql2.'<br>';
			//echo '<pre>'; print_r($row2); echo '</pre>';
			if($row2['mem_group_type'] == '1'){
				$sql3 = "SELECT
							t1.mem_group_id,
							t1.mem_group_parent_id,
							t1.mem_group_type,
							t1.mem_group_name,
							t1.id AS department_id,
							t2.id AS faction_id,
							t2.mem_group_name AS faction_name,
							t3.id AS level_id,
							t3.mem_group_name AS level_name
						FROM
							coop_mem_group AS t1
							LEFT JOIN coop_mem_group AS t2 ON t1.id = t2.mem_group_parent_id AND t2.mem_group_name = 'ไม่ระบุ'
							LEFT JOIN coop_mem_group AS t3 ON t2.id = t3.mem_group_parent_id AND t3.mem_group_name = 'ไม่ระบุ'
						WHERE
							t1.mem_group_id = '".$value['department_code']."'";
				$rs3 = $mysqli->query($sql3);
				$row3 = $rs3->fetch_assoc();
				$data[$key]['department'] = $row3['department_id'];			
				$data[$key]['faction'] = $row3['faction_id'];
				$data[$key]['level'] = $row3['level_id'];
			}
			
			if($row2['mem_group_type'] == '2'){
				$sql4 = "SELECT
							t1.mem_group_id,
							t1.mem_group_parent_id,
							t1.mem_group_type,	
							t2.id AS department_id,	
							t2.mem_group_name AS department_name,		
							t1.id AS faction_id,
							t1.mem_group_name AS faction_name,	
							t3.id AS level_id,
							t3.mem_group_name AS level_name
						FROM
							coop_mem_group AS t1
							LEFT JOIN coop_mem_group AS t2 ON t1.mem_group_parent_id = t2.id 
							LEFT JOIN coop_mem_group AS t3 ON t1.id = t3.mem_group_parent_id AND t3.mem_group_name = 'ไม่ระบุ'
						WHERE
							t1.mem_group_id = '".$value['department_code']."'";
				$rs4 = $mysqli->query($sql4);
				$row4 = $rs4->fetch_assoc();
				$data[$key]['department'] = $row4['department_id'];			
				$data[$key]['faction'] = $row4['faction_id'];
				$data[$key]['level'] = $row4['level_id'];
			}
			
			if($row2['mem_group_type'] == '3'){
				$sql5 = "SELECT
							t1.mem_group_id,
							t1.mem_group_parent_id,
							t1.mem_group_type,	
							t3.id AS department_id,	
							t3.mem_group_name AS department_name,			
							t2.id AS faction_id,
							t2.mem_group_name AS faction_name,		
							t1.id AS level_id,
							t1.mem_group_name AS level_name
						FROM
							coop_mem_group AS t1
							LEFT JOIN coop_mem_group AS t2 ON t1.mem_group_parent_id = t2.id 
							LEFT JOIN coop_mem_group AS t3 ON t2.mem_group_parent_id = t3.id
						WHERE
							t1.mem_group_id = '".$value['department_code']."'";
				$rs5 = $mysqli->query($sql5);
				$row5 = $rs5->fetch_assoc();
				$data[$key]['department'] = $row5['department_id'];			
				$data[$key]['faction'] = $row5['faction_id'];
				$data[$key]['level'] = $row5['level_id'];
			}
			
		
			$sql_update= "UPDATE coop_mem_apply SET 
				department = '".$data[$key]['department']."',
				faction = '".$data[$key]['faction']."',
				level = '".$data[$key]['level']."'
				WHERE member_id = '".$data[$key]['member_id']."'
				;";
			echo $sql_update.'<br>';
			$run++;
		}	
	}
	
	//echo"<pre>";print_r($data);
	exit;
?>