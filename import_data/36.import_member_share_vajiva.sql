-------------- UPDATE_MEM_APPLY --------------
INSERT INTO vajiracoop_sys.coop_mem_apply (apply_type_id,apply_date,member_date,mem_type,member_id,prename_id,firstname_th,lastname_th,sex,birthday,id_card,mem_group_id,tel,office_tel,father_name,mother_name,insurance_id,marry_status,nationality,position_id,work_date,retry_date,approve_date,note)
SELECT APP_TYPE,APPLY_DATE,APPROVE_DATE, MEM_TYPE,LPAD(MEMBERSHIP_NO, 6, '0') as MEMBERSHIP_NO,PRENAME_CODE,MEMBER_NAME,MEMBER_SURNAME,SEX,DATE_OF_BIRTH,ID_CARD,MEMBER_GROUP_NO,PHONE,WORK_TELEPHONE,FATHER,MOTHER,INSURANCE_NO,MARRIAGE_STATUS,NATIONALITY_CODE,POSITION_CODE,WORKING_DATE,RETIRE_DATE,APPROVE_DATE,REMARK FROM sc_mem_m_membership_registered WHERE LPAD(MEMBERSHIP_NO, 6, '0') NOT IN (
SELECT RIGHT(member_id, 6) FROM vajiracoop_sys.coop_mem_apply)

-- ทีใช้วชิระ
-- INSERT INTO vajiracoop_sys.coop_mem_apply (apply_type_id,apply_date,member_date,mem_type,member_id,prename_id,firstname_th,lastname_th,sex,birthday,
-- id_card,mem_group_id,tel,office_tel,father_name,mother_name,insurance_id,marry_status,nationality,position_id,work_date,retry_date,approve_date,note)
-- SELECT APP_TYPE,APPLY_DATE,APPROVE_DATE, MEM_TYPE,MEMBERSHIP_NO as MEMBERSHIP_NO,PRENAME_CODE,MEMBER_NAME,MEMBER_SURNAME,SEX,DATE_OF_BIRTH,
-- ID_CARD,MEMBER_GROUP_NO,PHONE,WORK_TELEPHONE,FATHER,MOTHER,INSURANCE_NO,MARRIAGE_STATUS,NATIONALITY_CODE,POSITION_CODE,WORKING_DATE,RETIRE_DATE,APPROVE_DATE,REMARK
-- FROM sc_mem_m_membership_registered
-- WHERE MEMBERSHIP_NO NOT IN (
-- SELECT member_id FROM vajiracoop_sys.coop_mem_apply)
-------------- UPDATE_MEM_APPLY_ID --------------
set @lastid = 202112000000;
UPDATE vajiracoop_sys.coop_mem_apply SET mem_apply_id = (@lastid := @lastid+1) WHERE mem_apply_id is null

-------------- UPDATE_MEM_APPLY_STATUS --------------
UPDATE `vajiracoop_sys`.`coop_mem_apply` SET `member_status` = 1 WHERE `member_status` is null

UPDATE vajiracoop_sys.coop_mem_apply A
INNER JOIN (
SELECT
	LPAD(MEMBERSHIP_NO, 6, '0') MEMBERSHIP_NO,
	T2.MEMBER_STATUS_CODE,
	T2.MEMBER_STATUS_NAME
FROM
	sc_mem_m_membership_registered T1
	INNER JOIN sc_mem_m_ucf_member_status T2 ON T1.MEMBER_STATUS_CODE = T2.MEMBER_STATUS_CODE
WHERE
	T1.MEMBER_STATUS_CODE = 3 ) B ON A.member_id=B.MEMBERSHIP_NO
	SET A.mem_type = 2,
	A.member_status = 2

-- ทีใช้วชิระ
-- UPDATE vajiracoop_sys.coop_mem_apply A
-- INNER JOIN (
-- SELECT
-- 	MEMBERSHIP_NO MEMBERSHIP_NO,
-- 	T2.MEMBER_STATUS_CODE,
-- 	T2.MEMBER_STATUS_NAME
-- FROM
-- 	sc_mem_m_membership_registered T1
-- 	INNER JOIN sc_mem_m_ucf_member_status T2 ON T1.MEMBER_STATUS_CODE = T2.MEMBER_STATUS_CODE
-- WHERE
-- 	T1.MEMBER_STATUS_CODE = 3 ) B ON A.member_id=B.MEMBERSHIP_NO
-- 	SET A.mem_type = 2,
-- 	A.member_status = 2

-------------- UPDATE_MEM_APPLY_TEL --------------
UPDATE vajiracoop_sys.coop_mem_apply t1
INNER JOIN vajire_20211116.sc_mem_m_member_work_info t2 ON RIGHT(t1.member_id, 5) = t2.MEMBERSHIP_NO
SET t1.salary = t2.SALARY_AMOUNT

-- ทีใช้วชิระ
-- UPDATE vajiracoop_sys.coop_mem_apply t1
-- INNER JOIN vajire_20211116.sc_mem_m_member_work_info t2 ON t1.member_id = t2.MEMBERSHIP_NO
-- SET t1.salary = t2.SALARY_AMOUNT

-------------- UPDATE_MEM_APPLY_SHARE --------------
-- //////////// SEE MEMBER SHARE MONTH ////////////
UPDATE `vajiracoop_sys`.`coop_mem_apply` SET `share_month` = 0 WHERE share_month is null

SELECT t1.share_month , t2.SHARE_AMOUNT FROM vajiracoop_sys.coop_mem_apply t1
INNER JOIN sc_mem_m_share_mem t2 ON RIGHT ( t1.member_id, 5 ) = t2.MEMBERSHIP_NO
WHERE t1.share_month <> t2.SHARE_AMOUNT

-- //////////// UPDATE MEMBER SHARE MONTH ////////////

UPDATE vajiracoop_sys.coop_mem_apply t1
INNER JOIN sc_mem_m_share_mem t2 ON RIGHT member_id = t2.MEMBERSHIP_NO
SET t1.share_month = t2.SHARE_AMOUNT


-------------- UPDATE_MEM_APPLY_GROUP --------------
-- UPDATE vajiracoop_sys.coop_mem_apply A
-- INNER JOIN (SELECT
-- mem_group_id,
-- @depart := IF(p3 = '' OR p3 IS NULL,IF( p2 = '' OR p2 IS NULL, p1, p2 ),  p3) as `depart`,
-- @faction := IF(p2 = '' OR p2 IS NULL OR @depart = p2  , IF(@depart = p1, null, p1 ), p2 ) as `faction`,
-- IF(p1 = @faction OR p1 = @depart, NULL, p1) as `level`
-- FROM (
-- SELECT
-- LPAD(t1.id - 1, 4, '0') as mem_group_id,
-- t1.id as `p1`,
-- t1.mem_group_parent_id as `p2`,
-- t2.mem_group_parent_id as `p3`
-- FROM coop_mem_group t1
-- LEFT JOIN coop_mem_group t2 ON t1.mem_group_parent_id=t2.id
-- LEFT JOIN coop_mem_group t3 ON t2.mem_group_parent_id=t3.id
-- ORDER BY t1.id) T1 ) B ON A.mem_group_id = B.mem_group_id
-- SET
-- A.department = B.depart,
-- A.faction =  B.faction,
-- A.`level` = B.`level`

