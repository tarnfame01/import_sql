<?php
//header("Content-Type: application/vnd.ms-excel"); // ประเภทของไฟล์
//header('Content-Disposition: attachment; filename="myexcel.xls"'); //กำหนดชื่อไฟล์
//header("Content-Type: application/force-download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Transfer-Encoding: binary");
//header("Content-Length: ".filesize("myexcel.xls"));

@readfile($filename);
set_time_limit (600);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('document/7spktcoop_average_dividend_account_t.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
$sheetData = $objPHPExcel->setActiveSheetIndex(2);
$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
//echo $yeartitle."<br>";
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

$data = array();
$i=0;
foreach($sheetData as $key => $value){
    if($key >= 3){
//		if($key >= 4 && $key < 10){
        if($value['A'] == ''){
            break;
        }

        $data[$i]['member_id'] = trim($value['B']);
        $data[$i]['member_id'] = sprintf("%06d",$data[$i]['member_id']);
        $data[$i]['deposit_balance'] = trim($value['F']);
        $i++;
    }
}
//echo '<pre>';print_r($data);exit;

$sql_account_id = "SELECT t1.account_id, t1.mem_id 
FROM coop_maco_account as t1
INNER JOIN coop_deposit_type_setting as t2 ON t1.type_id = t2.type_id
WHERE t2.type_code = '26'";
$rs_account_id = $mysqli->query($sql_account_id);
$check_account_id = array();
while($row_account_id = $rs_account_id->fetch_assoc()){
    $sql_transaction_balance = "
        SELECT t1.transaction_balance, t1.transaction_no_in_balance, t1.transaction_time
        FROM coop_account_transaction as t1
        WHERE t1.account_id = '".$row_account_id['account_id']."'
        ORDER BY t1.transaction_time DESC, t1.transaction_id DESC LIMIT 1";
//    echo $sql_transaction_balance;exit;
    $rs_transaction_balance = $mysqli->query($sql_transaction_balance);
    $transaction_balance = '---';
    while($row_transaction_balance = $rs_transaction_balance->fetch_assoc()){
        $transaction_balance = $row_transaction_balance['transaction_balance'];
        $transaction_no_in_balance = $row_transaction_balance['transaction_no_in_balance'];
        $transaction_time = $row_transaction_balance['transaction_time'];
    }

    $check_account_id[$row_account_id['mem_id']]['account_id'] = $row_account_id['account_id'];
    $check_account_id[$row_account_id['mem_id']]['transaction_balance'] = $transaction_balance;
    $check_account_id[$row_account_id['mem_id']]['transaction_no_in_balance'] = $transaction_no_in_balance;
    $check_account_id[$row_account_id['mem_id']]['transaction_time'] = $transaction_time;
}

?>
<!--<table border="1">-->
<!--    <tr>-->
<!--        <td>ลำดับ</td>-->
<!--        <td>เลขทะเบียนสมาชิก</td>-->
<!--        <td>เงินฝาก</td>-->
<!--        <td>บัญชี</td>-->
<!--        <td>ยอดล่าสุด</td>-->
<!--        <td>ยอดเงินที่ไม่คิดดอกเบี้ย</td>-->
<!--        <td>เวลาล่าสุด</td>-->
<!--    </tr>-->
<!--    --><?php //foreach ($data as $key => $value) {?>
<!--        <tr>-->
<!--            <td>--><?php //echo $key + 1; ?><!--</td>-->
<!--            <td>--><?php //echo $value['member_id']; ?><!--</td>-->
<!--            <td>--><?php //echo number_format($value['deposit_balance'], 2); ?><!--</td>-->
<!--            <td>--><?php //echo $check_account_id[$value['member_id']]['account_id']; ?><!--</td>-->
<!--            <td>--><?php //echo $check_account_id[$value['member_id']]['transaction_balance'] ; ?><!--</td>-->
<!--            <td>--><?php //echo $check_account_id[$value['member_id']]['transaction_no_in_balance']; ?><!--</td>-->
<!--            <td>--><?php //echo $check_account_id[$value['member_id']]['transaction_time']; ?><!--</td>-->
<!--        </tr>-->
<!--        --><?php
//    }
//    ?>
<!--</table>-->
<?php
//exit;
foreach($data as $key => $value){
    $date_now = date("Y-m-d H:i:s", strtotime('+6 hour'));
    $value['deposit_balance'] = sprintf("%.2f",$value['deposit_balance']);
    $new_transaction_balance = $check_account_id[$value['member_id']]['transaction_balance'] + $value['deposit_balance'];
    $transaction_no_in_balance = $check_account_id[$value['member_id']]['transaction_no_in_balance'];
    $account_id = $check_account_id[$value['member_id']]['account_id'];
    if(!empty($transaction_no_in_balance)){
        $transaction_no_in_balance = "'".$transaction_no_in_balance."'";
    }else{
        $transaction_no_in_balance = "NULL";
    }
    $sql_update = "INSERT INTO `coop_account_transaction`(`transaction_time`, `transaction_list`, `transaction_withdrawal`, `transaction_deposit`, `transaction_balance`, `user_id`, `account_id`, `print_status`, `print_number_point`, `book_number`, `transaction_id`, `transaction_no_in_balance`, `loan_id`, `receipt_id`, `member_id_atm`, `ref_no`, `cancel_status`, `cancel_ref_transaction_id`, `deposit_balance`, `deposit_interest_balance`, `ref_transaction_id`, `interest_period`, `fixed_deposit_status`, `date_end_saving`, `fixed_deposit_type`, `day_cal_interest`, `permission_by_user`, `createtime`, `transaction_text`, `transaction_date_atm`, `company_atm`) VALUES ('".$date_now."', 'YPF', 0, '".$value['deposit_balance']."', '".$new_transaction_balance."', 'SYSTEM', '".$account_id."', NULL, NULL, NULL, NULL, ".$transaction_no_in_balance.", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);";
    echo $sql_update . '<br>';
}

?>