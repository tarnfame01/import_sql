<?php
//header("Content-Type: application/vnd.ms-excel"); // ประเภทของไฟล์
//header('Content-Disposition: attachment; filename="myexcel.xls"'); //กำหนดชื่อไฟล์
//header("Content-Type: application/force-download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Transfer-Encoding: binary");
//header("Content-Length: ".filesize("myexcel.xls"));

@readfile($filename);
set_time_limit (60);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('document/27venus_member.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');

$sheetData = $objPHPExcel->setActiveSheetIndex(0);
$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

$data = array();
$i = 0;
//echo '<pre>'; print_r($sheetData);exit;
$date_now = date("Y/m/d H:i:s", strtotime('+6 hour'));
$master_id = '8';
$year = '2021';
$dividend_percent = 4;
$average_percent = 12;
foreach ($sheetData as $key => $value) {
    if ($key >= 2) {
//		if($key >= 18 && $key <= 20){
        if ($value['A'] == '') {
            break;
        }

//        $data[$i]['member_id'] = sprintf("%06d", trim($value['A']));
        $data[$i]['member_id'] = trim($value['A']);
        $data[$i]['mem_apply_id'] = trim($value['B']);
        $data[$i]['id_card'] = trim($value['C']);
        $data[$i]['prename_id'] = trim($value['D']);
        $data[$i]['firstname_th'] = trim($value['E']);
        $data[$i]['lastname_th'] = trim($value['F']);
        $data[$i]['sex'] = trim($value['G']);
        $data[$i]['birthday'] = $value['H'];
        $data[$i]['member_date'] = $value['I'];
        $data[$i]['mem_type'] = trim($value['J']);
        $data[$i]['marry_status'] = trim($value['K']);
        $data[$i]['salary'] = trim($value['L']);
        $data[$i]['tel'] = trim($value['M']);
        $data[$i]['address_no'] = trim($value['W']);
        $data[$i]['address_moo'] = trim($value['X']);
        $data[$i]['address_village'] = trim($value['Y']);
        $data[$i]['address_road'] = trim($value['Z']);
        $data[$i]['address_soi'] = trim($value['AA']);
        $data[$i]['district_id'] = trim($value['AB']);
        $data[$i]['amphur_id'] = trim($value['AC']);
        $data[$i]['province_id'] = trim($value['AD']);
        $data[$i]['zipcode'] = trim($value['AE']);
        $data[$i]['department'] = 1;
        $data[$i]['faction'] = trim($value['AG']);
        $data[$i]['level'] = trim($value['AH']);
        $i++;
    }
}
$group_arr = array();
foreach ($data as $key => $value){
//    $group_arr['faction'][$value['faction']] = $value['faction'];
//    $group_arr['level'][$value['level']] = $value['level'];
    $group_arr[$value['faction']]['level'][$value['level']] = $value['level'];
}
ksort($group_arr);
foreach ($group_arr as $key => $value){
    ksort($group_arr[$key]['level']);
}
$i = 1;
foreach ($group_arr as $key => $value){
    $i++;
    $group_arr[$key]['ID'] = $i;
    foreach ($value['level'] as $key2 => $value2){
        $i++;
        $group_arr[$key]['level'][$key2] = $i;
    }
}
//echo '<pre>'; print_r($group_arr);echo '</pre>';
foreach ($group_arr as $faction => $detail){
    $sql = "INSERT INTO `coop_mem_group`(`id`, `mem_group_id`, `mem_group_parent_id`, `mem_group_type`, `short_mem_group_name`, `mem_group_name`, `mem_group_full_name`, `debtor_agent_group`, `mem_group_parent_group`, `mem_group_branch`, `manager_group`, `address_no`, `address_moo`, `address_road`, `address_soi`, `province_id`, `amphur_id`, `district_id`, `zipcode`, `tel`, `fax`, `bank_group_id`, `bank_branch_id`) VALUES ('".$detail['ID']."', '".$faction."', '1', '2', NULL, '".$faction."', '".$faction."', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);";
//    echo $sql.'<br>';
    foreach ($detail['level'] as $level => $level_id){
        $sql = "INSERT INTO `coop_mem_group`(`id`, `mem_group_id`, `mem_group_parent_id`, `mem_group_type`, `short_mem_group_name`, `mem_group_name`, `mem_group_full_name`, `debtor_agent_group`, `mem_group_parent_group`, `mem_group_branch`, `manager_group`, `address_no`, `address_moo`, `address_road`, `address_soi`, `province_id`, `amphur_id`, `district_id`, `zipcode`, `tel`, `fax`, `bank_group_id`, `bank_branch_id`) VALUES ('".$level_id."', '".$level."', '".$detail['ID']."', '3', NULL, '".$level."', '".$level."', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);";
//        echo $sql.'<br>';
    }
}
//echo '<pre>'; print_r($data);echo '</pre>';
//exit;
foreach($data as $key => $value){

    if($value['sex'] == 'ชาย'){
        $value['sex'] = 'M';
    }else if($value['sex'] == 'หญิง'){
        $value['sex'] = 'F';
    }else{
        $value['sex'] = '';
    }

    $value['prename_id'] = str_replace('นส.',"น.ส.",$value['prename_id']);
    $prename_id = '';
    $prename_sql = "SELECT * FROM `venus_sys`.`coop_prename` where prename_full = '{$value['prename_id']}'"; // RUN 2
    $row_prename = $mysqli->query($prename_sql)->fetch_assoc();
    if(!empty($row_prename)){
        $prename_id = $row_prename['prename_id'];
    }else{
        $prename_sql = "SELECT * FROM `venus_sys`.`coop_prename` where prename_short = '{$value['prename_id']}'"; // RUN 2
        $row_prename = $mysqli->query($prename_sql)->fetch_assoc();
        if(!empty($row_prename)){
            $prename_id = $row_prename['prename_id'];
        }
    }
//    if(empty($prename_id)){
//        echo $value['prename_id'];
//        exit;
//    }
//    echo '<pre>'; print_r($row_prename);echo '</pre>';

    if($value['marry_status'] == 'โสด'){
        $value['marry_status'] = 1;
    }else if($value['marry_status'] == 'สมรส'){
        $value['marry_status'] = 2;
    }else if($value['marry_status'] == 'หย่า'){
        $value['marry_status'] = 3;
    }else if($value['marry_status'] == 'หม้าย' || $value['marry_status'] == 'ม่าย'){
        $value['marry_status'] = 4;
    }else if($value['marry_status'] == 'แยกกันอยู่'){
        $value['marry_status'] = 5;
    }else{
        $value['marry_status'] = '';
    }

    $province_sql = "SELECT * FROM `venus_sys`.`coop_province` WHERE `province_name` LIKE '%{$value['province_id']}%'"; // RUN 2
    $row_province = $mysqli->query($province_sql)->fetch_assoc();

    $amphur_sql = "SELECT * FROM `venus_sys`.`coop_amphur` WHERE `amphur_name` LIKE '%{$value['amphur_id']}%' AND `province_id` = '{$row_province['province_id']}'"; // RUN 2
    $row_amphur = $mysqli->query($amphur_sql)->fetch_assoc();

    $district_sql = "SELECT * FROM `venus_sys`.`coop_district` WHERE `district_name` LIKE '%{$value['district_id']}%'"; // RUN 2
    $row_district = $mysqli->query($district_sql)->fetch_assoc();

    $faction_id = $group_arr[$value['faction']]['ID'];
    $level_id = $group_arr[$value['faction']]['level'][$value['level']];

//    echo '<pre>'; print_r($value);echo '</pre>';
    $sql_update= "INSERT coop_mem_apply SET
        member_id = '".$value['member_id']."',
        mem_apply_id = '".$value['mem_apply_id']."',
        id_card = '".$value['id_card']."',
        prename_id = '".$prename_id."',
        firstname_th = '".$value['firstname_th']."',
        lastname_th = '".$value['lastname_th']."',
        sex = '".$value['sex']."',
        birthday = '".$value['birthday']."',
        member_date = '".$value['member_date']."',
        marry_status = '',
        salary = '".$value['salary']."',
        tel = '".$value['tel']."',
        address_no = '".$value['address_no']."',
        address_moo = '".$value['address_moo']."',
        address_village = '".$value['address_village']."',
        address_road = '".$value['address_road']."',
        address_soi = '".$value['address_soi']."',
        province_id = '".$row_province['province_id']."',
        amphur_id = '".$row_amphur['amphur_id']."',
        district_id = '".$value['district_id']."',
        zipcode = '".$value['zipcode']."',
        department = '".$value['department']."',
        faction = '".$faction_id."',
        level = '".$level_id."'
        ;";
    echo $sql_update.'<br>';
//    exit;
}

$sql_update = "UPDATE `coop_mem_apply` SET `mem_type` = 1 WHERE `mem_type` IS NULL;";
echo $sql_update.'<br>';
$sql_update = "UPDATE `coop_mem_apply` SET `member_status` = 1 WHERE `member_status` IS NULL;";
echo $sql_update.'<br>';


