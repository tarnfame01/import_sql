<?php
//header("Content-Type: application/vnd.ms-excel"); // ประเภทของไฟล์
//header('Content-Disposition: attachment; filename="myexcel.xls"'); //กำหนดชื่อไฟล์
//header("Content-Type: application/force-download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Transfer-Encoding: binary");
//header("Content-Length: ".filesize("myexcel.xls"));

@readfile($filename);
set_time_limit (60);
include '../connect.inc.php';
include '../../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('../document/29srusct_increase_share.xlsx');
$sheetData = $objPHPExcel->setActiveSheetIndex(0);
$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
// --------------------------------------------------------------------------------------------------------------------------------- //

$share_rule_sql = "SELECT * FROM `sructsys_com_bk65-01-18`.`coop_share_rule` WHERE `mem_type_id` = '1' order by salary_rule DESC"; // RUN 1
$rs_share_rule = $mysqli->query($share_rule_sql);
$coop_share_rule = array();

while($row_share_rule = $rs_share_rule->fetch_assoc()){
    $coop_share_rule[] = $row_share_rule;
}
//echo '<pre>'; print_r($coop_share_rule);echo '</pre>';
//exit;
// ส่วนของหุ้น
 $share_register_sql = "SELECT t1.member_id, t1.share_date_period_1, t3.share_date, t1.share_month, t1.salary, t1.employee_id, t1.mem_type_id, t2.member_id as check_member_id
FROM `sructsys_com_bk65-01-18`.coop_mem_apply as t1
LEFT JOIN (
	SELECT * 
	FROM `sructsys_com_bk65-01-18`.coop_change_share 
	WHERE change_share_status != '3'
	GROUP BY member_id
) as t2 ON t1.member_id = t2.member_id
LEFT JOIN (
	SELECT member_id, share_date 
	FROM `sructsys_com_bk65-01-18`.coop_mem_share 
	GROUP BY member_id
) as t3 ON t1.member_id = t3.member_id
WHERE t1.member_id IS NOT NULL AND t1.member_id != '' 
-- AND t2.member_id is null AND (t1.share_date_period_1 IS NOT NULL AND t3.share_date IS NOT NULL)"; // RUN 1
$rs_share_register = $mysqli->query($share_register_sql);
$coop_share_register = array();


foreach($sheetData as $key => $value){
    if($key >= 2){
        if($value['A'] == ''){
            break;
        }
        $employee_id = trim($value['B']);
        $coop_share_register[$employee_id]['new_salary'] = $value['E'];
        $coop_share_register[$employee_id]['de_'] = $value['E'];
    }
}
//echo '<pre>'; print_r($coop_share_register);exit;

while($row_share_register = $rs_share_register->fetch_assoc()){
    if(!empty($row_share_register['share_month'])){
        if(!empty($coop_share_register[$row_share_register['employee_id']])){
            $new_salary = $coop_share_register[$row_share_register['employee_id']]['new_salary'];
            //employee_id
            $coop_share_register[$row_share_register['employee_id']] = $row_share_register;
            $coop_share_register[$row_share_register['employee_id']]['new_salary'] = $new_salary;
        }
    }
}
//echo '<pre>'; print_r($coop_share_register);exit;
$date = '2022-01-18 00:00:00';
$total = 0;
//echo '<table>';
foreach ($coop_share_register as $employee_id => $data){
//    echo '<pre>'; print_r($data);echo '</pre>';
    $new_share_month = 0;
    if(!empty($coop_share_register[$employee_id]['de_'])){
        echo 'de_'.'de_'.'de_'.'de_';
        exit;
    }

    if(empty($data['check_member_id'])){
        // เพิ่ม/ลดหุ้น
    }
    foreach($coop_share_rule as $key => $value){
        if( $data['new_salary'] >= $value['salary_rule'] ){
            $new_share_month = $value['share_salary']*10;
            break;
        }
    }
    if($data['share_month'] < $new_share_month){
        if(empty($new_share_month)){$new_share_month = 0;}
        if(empty($data['share_month'])){@$data['share_month'] = 0;}
        $total += (@$new_share_month - @$data['share_month']);
//        echo '<tr>';
//        echo '<td>'.$data['member_id'].'</td>';
//        echo '<td>'.($new_share_month - $data['share_month']).'</td>';
//        echo '</tr>';
        $sql = "UPDATE `coop_mem_apply` SET 
            `share_month` = '{$new_share_month}'
            WHERE `member_id` = '{$data['member_id']}';";
//        echo $sql.'<br>';

        $share_date = '';
        if(empty($data['check_member_id'])){
            if(!empty($data['share_date_period_1'])){
                $share_date = $data['share_date_period_1'];
            }elseif(!empty($data['share_date'])){
                $share_date = $data['share_date'];
            }else{
                $share_date = $date;
                echo '$share_date--';
                exit;
            }
            $share_month = $data['share_month'] / 10;
            $sql = "INSERT INTO `coop_change_share` SET 
            member_id='{$data['member_id']}', 
            admin_id='1', 
            change_type = 'increase',
            change_value = '{$share_month}',
            change_value_price = '{$data['share_month']}',
            share_value = '10',
            create_date = '{$share_date}',
            change_share_status = '1',
            active_date = '{$share_date}';";
            echo $sql.'<br>';
        }

        $share_month = $new_share_month / 10;
        $sql = "INSERT INTO `coop_change_share` SET 
        member_id='{$data['member_id']}', 
        admin_id='1', 
        change_type = 'increase',
        change_value = '{$share_month}',
        change_value_price = '{$new_share_month}',
        share_value = '10',
        create_date = '{$date}',
        change_share_status = '1',
        active_date = '{$date}';";
        echo $sql.'<br>';
    }
    if($data['salary'] != $data['new_salary']){
        $sql = "INSERT INTO `coop_member_data_history` SET 
            member_id='{$data['member_id']}', 
            input_name = 'salary',
            old_value = '{$data['salary']}',
            new_value = '{$data['new_salary']}',
            user_id = '1',
            created_at = '{$date}';";
//        echo $sql.'<br>';

        if($data['share_month'] > $new_share_month){
            // เอาหุ้นคืน
//            echo '-- '.$new_share_month.'<br>';
            $sql = "UPDATE `coop_mem_apply` SET
            `share_month` = '{$data['share_month']}'
            WHERE `member_id` = '{$data['member_id']}';";
            echo $sql.'<br>';
            echo '<hr>';
        }
        //
    }
    $sql = "UPDATE `coop_mem_apply` SET 
            `salary` = '{$data['new_salary']}'
            WHERE `member_id` = '{$data['member_id']}';";
//    echo $sql.'<br>';
//    echo '<hr>';
//    exit;

}
//echo '</table>';
//echo number_format($total,2);

/*

UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 4000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 2000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 7000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 2500
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1100
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 6000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 5000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 5000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 2100
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 3000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 5000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1200
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1500
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1600
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1200
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1500
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 2000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 2000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 2000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1500
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 2000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 2000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 3000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 2000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 5000
UPDATE `sructsys_com_bk65-01-18`.`coop_mem_apply` SET `share_month` = 1000


 '00605';
 '03328';
 '11773';
 '13129';
 '00511';
 '09924';
 '10918';
 '11658';
 '10653';
 '05315';
 '12542';
 '02846';
 '12540';
 '07041';
 '09365';
 '10906';
 '09537';
 '12055';
 '13037';
 '12076';
 '04495';
 '10721';
 '07120';
 '08979';
 '12830';
 '07527';
 'S0317';
 '12041';
 '12381';
 'S0420';
 '12752';
 '10011';
 '12941';
 '10006';
 '12324';
 '09520';
 'S0156';


