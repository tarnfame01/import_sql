<?php
//header("Content-Type: application/vnd.ms-excel"); // ประเภทของไฟล์
//header('Content-Disposition: attachment; filename="myexcel.xls"'); //กำหนดชื่อไฟล์
//header("Content-Type: application/force-download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Transfer-Encoding: binary");
//header("Content-Length: ".filesize("myexcel.xls"));

@readfile($filename);
set_time_limit (60);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('document/9mmth_average_dividend_varchar.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
$sheetData = $objPHPExcel->setActiveSheetIndex(1);
$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
//echo $yeartitle."<br>";
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

$data = array();
$i=0;

//echo '<pre>';
//print_r($sheetData);
//exit;
foreach($sheetData as $key => $value){
    if($key >= 3){
        if($value['A'] == ''){
            break;
        }

        $member_id = trim($value['A']);
        $data[$member_id]['member_id'] = $member_id;
        $data[$member_id]['fullname'] = trim($value['B']);
        $data[$member_id]['dividend'] = trim($value['C']);
        $data[$member_id]['average'] = trim($value['D']);
        $i++;
    }
}

/*
foreach ($data as $key => $value) {
    $sql = "INSERT INTO `mmthcoop_sys`.`coop_dividend_average`(`member_id`, `master_id`, `year`, `transfer_gift_date`, `transfer_date`, `dividend_value`, `average_return_value`, `dividend_percent`, `average_percent`, `gift_varchar`, `id`, `sum_dividend`, `sum_return`, `sum_divide_return`, `date_create`) VALUES ('".$value['member_id']."', 4, 2021, NULL, NULL, ".$value['dividend'].", ".$value['average'].", 4, 5, 0, NULL, NULL, NULL, NULL, '2021-03-24 10:22:59');";
    echo $sql.'<br>';
}
exit;
*/

$sql_dividend_average = "SELECT * FROM coop_dividend_average WHERE master_id = '4' ORDER BY member_id";
$rs_dividend_average = $mysqli->query($sql_dividend_average);
$dividend_average_arr = array();
while($row_dividend_average = $rs_dividend_average->fetch_assoc()){
    $dividend_average_arr[$row_dividend_average['member_id']] = $row_dividend_average;
}

//echo '<pre>';
//print_r($dividend_average_arr['00002']);
//echo '</pre>';


echo '<table border="1">';
foreach ($dividend_average_arr as $key => $value) {
    $member_id = $value['member_id'];
    echo '<td>'.$member_id.'</td>';
    echo '<td></td>';
    echo '<td></td>';
    echo '<td>'.$value['dividend_value'].'</td>';
    echo '<td>'.$value['average_return_value'].'</td>';
    echo '<td>'.($value['dividend_value']+$value['average_return_value']).'</td>';
    echo '</tr>';
    if(!empty($data[$member_id])){
//        echo '<tr>';
        if($data[$member_id]['dividend'] == '-'){
            $data[$member_id]['dividend'] = 0;
        }
        if($data[$member_id]['average'] == '-'){
            $data[$member_id]['average'] = 0;
        }
//            $sql = "UPDATE `coop_dividend_average` SET `dividend_value` = ".$data[$member_id]['dividend'].", `average_return_value` = ".$data[$member_id]['average']." WHERE `master_id` = '4' AND member_id = '".$member_id."';";
//            echo $sql.'<br>';
    }else{
        //ปันผลเกิน
//        echo $member_id.'<br>';
    }
}
echo '</table>';

