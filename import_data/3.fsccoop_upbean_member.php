<?php
//header("Content-Type: application/vnd.ms-excel"); // ประเภทของไฟล์
//header('Content-Disposition: attachment; filename="myexcel.xls"'); //กำหนดชื่อไฟล์
//header("Content-Type: application/force-download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
//header("Content-Transfer-Encoding: binary");
//header("Content-Length: ".filesize("myexcel.xls"));

@readfile($filename);
set_time_limit (60);
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('document/16fsccoop_upbean_member.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
//2 = ธค
$sheetData = $objPHPExcel->setActiveSheetIndex(0);
$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
//echo '<pre>'; print_r($sheetData);exit;
//	exit;
$data = array();
$i=0;
echo '<pre>';
foreach($sheetData as $key => $value){
    if($key >= 3 && $key <= 60){

        $data[$i]['i'] = trim($value['A']);
        $data[$i]['fullname'] = trim($value['B']);
        $data[$i]['member_id'] = trim($value['C']);
        $data[$i]['share_collect_value'] = trim($value['D']);
        $i++;

    }
}
?>
<!--
// ส่วนนำเข้าสมาชิก
<table border="1">
    <tr>
        <td>ลำดับ</td>
        <td>ชื่อสหกรณ์</td>
        <td>เลขทะเบียน</td>
        <td>ยกมา</td>
        <td>เพิ่มระหว่างปี</td>
        <td>ลดระหว่างปี</td>
        <td>คงเหลือวันสิ้นปี</td>
    </tr>
    <?php foreach ($data as $key => $value) { ?>
    <tr>
        <td><?php echo $key+1;?></td>
        <td><?php echo $value['fullname'];?></td>
        <td><?php echo $value['member_id'];?></td>
        <td><?php echo $value['quoted'];?></td>
        <td><?php echo $value['add_year'];?></td>
        <td><?php echo $value['reduce_year'];?></td>
        <td><?php echo $value['balance'];?></td>
    </tr>
    <?php } ?>
</table>
-->
<?php
$date_now = date("Y/m/d h:i:s", strtotime('+6 hour'));

$master_id = '6';
foreach($data as $key => $value){
    $i = $key+1;
    $i6 = sprintf("%06d",$i);
    $member_id = sprintf("%06d",$value['member_id']);
    /*$sql_update= "INSERT coop_mem_apply SET
        mem_apply_id = '202012".$i6."',
        apply_type_id = '1',
        apply_date = '2020-12-23',
        is_fix_member_date = '0',
        member_date = '2020-12-23',
        mem_type = '1',
        mem_type_id = '1',
        member_id    = '".$member_id."',
        firstname_th    = '".$value['fullname']."',
        province_id    = '0',
        amphur_id    = '0',
        district_id    = '0',
        c_province_id    = '0',
        c_amphur_id    = '0',
        c_district_id    = '0',
        marry_status    = '1',
        department    = '1',
        faction    = '2',
        level    = '3',
        work_date = '0000-00-00',
        retry_date = '0000-00-00',
        birthday = '0000-00-00',
        salary    = '0',
        other_income    = '0',
        m_province_id    = '0',
        m_amphur_id    = '0',
        m_district_id    = '0',
        share_month    = '0',
        apply_status    = '0',
        dividend_bank_act_id    = '1',
        member_status    = '1',
        employee_id    = '".$member_id."',
        member_time    = '1',
        bank_type    = '2',
        court_writ = '0';";*/
    $sql_update = "UPDATE `coop_mem_apply` SET `firstname_th` = '{$value['fullname']}' WHERE `member_id` = '{$member_id}';";
    echo $sql_update.'<br>';
}

/*
// นำเข้าหุ้น 30/9
foreach($data as $key => $value){
    $i = $key+1;
    $i6 = sprintf("%06d",$i);
    $member_id = sprintf("%06d",$value['member_id']);
    $sql_update= "INSERT coop_mem_share SET
        member_id = '".$member_id."',
        admin_id = '1',
        share_type = 'SPA',
        share_date = '2021-09-30 00:00:00',
        share_status = '1',
        share_payable = '0',
        share_payable_value = '0',
        share_early = '".($value['share_collect_value']/10)."',
        share_early_value = '".$value['share_collect_value']."',
        share_collect = '".($value['share_collect_value']/10)."',
        share_collect_value = '".$value['share_collect_value']."',
        share_bill = null,
        share_bill_date = null,
        share_value = '10',
        share_period = null,
        pay_type = '0';";
    echo $sql_update.'<br>';
}

// นำเข้าปันผล 30/9
/*
foreach($data as $key => $value){
    $i = $key+1;
    $i6 = sprintf("%06d",$i);
    $member_id = sprintf("%06d",$value['member_id']);
    $sql_update= "INSERT coop_dividend_average SET
        member_id = '".$member_id."',
        master_id = '1',
        date_create = '2020-09-30 00:00:00',
        year = '2020',
        dividend_percent = '10',
        average_percent = '0',
        dividend_value = '0',
        average_return_value = '0',
        gift_varchar = '0';";
    //share_value = '".$value['balance']."',

//    echo $sql_update.'<br>';
}


/*
$data2 = array();
$i = 0;
$sheetData2 = $objPHPExcel->setActiveSheetIndex(11);
$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
$sheetData2 = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
foreach($sheetData2 as $key => $value){
    if($key >= 6 && $key <= 61) {
        $data2[$i]['member_id'] = trim($value['C']);
        $data2[$i]['dividend'] = trim($value['AJ']);
        $i++;
    }
}
foreach($data2 as $key => $value){
    $member_id = sprintf("%06d",$value['member_id']);
    $sql_update= "UPDATE coop_dividend_average SET dividend_value = '".$value['dividend']."' WHERE member_id = '".$member_id."';";
    echo $sql_update.'<br>';
}
*/
?>

