<?php
set_time_limit (60);
ini_set('memory_limit','10240M');
include 'connect.inc.php';
include '../PHPExcelReader/Classes/PHPExcel/IOFactory.php';
$objPHPExcel = PHPExcel_IOFactory::load('document/glocoop_member.xlsx');
$month_arr = array('มกราคม'=>'01','กุมภาพันธ์'=>'02','มีนาคม'=>'03','เมษายน'=>'04','พฤษภาคม'=>'05','มิถุนายน'=>'06','กรกฎาคม'=>'07','สิงหาคม'=>'08','กันยายน'=>'09','ตุลาคม'=>'10','พฤศจิกายน'=>'11','ธันวาคม'=>'12');
$month_short_arr = array('ม.ค.'=>'01','ก.พ.'=>'02','มี.ค.'=>'03','เม.ย.'=>'04','พ.ค.'=>'05','มิ.ย.'=>'06','ก.ค.'=>'07','ส.ค.'=>'08','ก.ย.'=>'09','ต.ค.'=>'10','พ.ย.'=>'11','ธ.ค.'=>'12');
$month_short_arr_eng = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
	$sheetData = $objPHPExcel->setActiveSheetIndex(0);
	$yeartitle = $objPHPExcel->getActiveSheet()->getTitle();
	//echo $yeartitle."<br>";
	$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

	$data = array();
	$i=0;
//    echo '<pre>'; print_r($sheetData); echo '</pre>';
//	exit;
	foreach($sheetData as $key => $value){

		if($key >= 2){
			if($value['A'] == ''){
				break;
			}
			//$data[$i]['main_department_id'] = substr(trim($value['A']),0,2);
			$data[$i]['runno'] = trim($value['A']);
			$data[$i]['id_card'] = trim($value['D']);
			$data[$i]['prename'] = trim($value['E']);
			$sql1 = "SELECT * FROM coop_prename WHERE prename_short = '".trim($value['E'])."'";
			$rs1 = $mysqli->query($sql1);
			$row1 = $rs1->fetch_assoc();

			$data[$i]['prename_id'] = $row1['prename_id'];

			$data[$i]['firstname_th'] = trim($value['F']);
			$data[$i]['lastname_th'] = trim($value['G']);
			$data[$i]['ISO'] = trim($value['H']);
			$data[$i]['share'] = trim($value['I']);
			$data[$i]['share_balance'] = trim($value['J']);
			$data[$i]['member_date'] = trim($value['K']);
			$data[$i]['mem_type'] = trim($value['L']);
			$data[$i]['mem_out'] = trim($value['M']);
			$data[$i]['address_no'] = trim($value['N']);
            $data[$i]['district'] = trim($value['O']);
			$data[$i]['amphur'] = trim($value['P']);
            $data[$i]['province'] = trim($value['Q']);
			$data[$i]['mobile'] = trim($value['R']);
			$data[$i]['salary'] = trim($value['S']);
			$i++;
		}
	}

	echo '<pre>';
//	print_r($data);
//	exit;
	$sql_mem = "SELECT id, member_id, id_card FROM coop_mem_apply";
	$rs_mem = $mysqli->query($sql_mem);
	$arr_member = array();
    $check_member = array();
	while($row_mem = $rs_mem->fetch_assoc()){
		$arr_member[$row_mem['id_card']]['id'] = $row_mem['id'];
		$arr_member[$row_mem['id_card']]['member_id'] = $row_mem['member_id'];
		$arr_member[$row_mem['id_card']]['id_card'] = $row_mem['id_card'];
        $check_member[$row_mem['id_card']] = $row_mem['id_card'];
	}

//	print_r($arr_member);
	$run = 15;
	foreach($data as $key => $value){
		if(in_array($value['id_card'],$check_member)){
//            print_r($value);
            $id = $arr_member[$value['id_card']]['id'];
            $set = '';
            if(!empty($value['mobile'])){
                $set .= "SET mobile = '".$value['mobile']."'";
//                echo $set;
            }

            if(!empty($value['salary'])){
                if($set != ''){
                    $set .= ' ,';
                }else{
                    $set .= 'SET ';
                }
                $set .= "salary = '".$value['salary']."'";
            }
            if($set != ''){
                $sql_mem = "UPDATE coop_mem_apply ".$set." WHERE `id` = '".$id."'";
                echo $arr_member[$value['id_card']]['member_id'].'<br>';
                echo $sql_mem.'<br>';
//                $rs_mem = $mysqli->query($sql_mem);
            }
		}
	}
	exit;
?>